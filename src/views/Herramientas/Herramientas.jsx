import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, Thumbnail, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
var moment = require('moment');
require('moment/locale/es');

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], newHerramienta: true, editTool: false, blocking: false, file: {},img:"", alert: null, imgName: "", textoprevio: "", textoprincipal: "", nombre: "", idherramienta: "" };
    this.toggleBox = this.toggleBox.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.editar = this.editar.bind(this);
    this.updateImage = this.updateImage.bind(this);
    this.onSubmitUpdateForm = this.onSubmitUpdateForm.bind(this);
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  toggleBox() {
    const { newHerramienta, editTool } = this.state;
    if (editTool === true) {
      this.setState({ editTool: false });
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "" });
    }
    this.setState({ newHerramienta: !newHerramienta });
  }
  editar() {
    this.setState({ editarImg: !this.state.editarImg })
  }
  updateImage() {
    if (this.state.file && this.state.file.base64) {
      this.updateImg(this.state.file);
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  getImg(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file: { extencion, base64 } })
  }
  setValues(data) {
    if (data.name) {
      const { name, textoprevio, textoprincipal, img, id } = data;
      this.setState({ nombre: name, textoprevio, textoprincipal, img, idherramienta: id });
      this.setState({ editTool: !this.state.editTool });
      this.toggleBox();
    } else {
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "", img: "" });
    }
  }
  filterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;
    if(row[id] !== undefined){
      if(String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }
  onSubmitUpdateForm() {
    const { nombre, textoprevio, textoprincipal, idherramienta } = this.state;
    if (nombre !== "" && textoprevio !== "" && textoprincipal !== "" ) {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { nombre, textoprevio, textoprincipal, idherramienta } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/tools/update", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("toolsForm").reset();
            this.onLoadTools();
            this.setState({ imgName: "", file: {}, nombre: "", textoprevio: "", textoprincipal: "" });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  onSubmitForm() {
    const { file, nombre, textoprevio, textoprincipal } = this.state;
    if (file !== {} && nombre !== "" && textoprevio !== "" && textoprincipal !== "") {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { file, nombre, textoprevio, textoprincipal } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/tools/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("toolsForm").reset();
            this.onLoadTools();
            this.setState({ imgName: "", file, nombre: "", textoprevio: "", textoprincipal: "" });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  onRemoveTool(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteTool(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  deleteTool(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idherramienta": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/tools/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onLoadTools(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  
  updateImg(file){
    this.setState({ blocking: !this.state.blocking });
    let fields = {
      data: { file,idherramienta :this.state.idherramienta}
    };
    axios.post("https://www.ideasqueactivanclaro.com.co/admin/tools/updateImg", fields)
      .then(res => {
        var resp = res.data.response;
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error === 0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
          setTimeout(this.onLoadTools(), 500);
          this.setState({img:file.base64});
          this.editar();          
        } else {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                danger
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"

              >
                {resp}
              </SweetAlert>
            )
          });
        }
      }).catch(error => {
        this.setState({ blocking: false});
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
}
  onLoadTools() {
    axios.get("https://www.ideasqueactivanclaro.com.co/app/general/tools/", {headers: {"x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI="}})
      .then(res => {
        var fecha;
        if (res.data.response) {
          const Herramientas = res.data.response.map((prop, key) => {
            fecha = moment(prop.fechaPublicacion).format("DD/MM/YYYY");
            return {
              id: prop.idherramienta,
              textoprevio: prop.textoprevio,
              textoprincipal: prop.textoprincipal,
              img: prop.img,
              name: prop.nombre,
              fecha: fecha,
              image: (
                <Col md={5}>
                  <Thumbnail src={prop.img} alt="242x200">
                  </Thumbnail>
                </Col>
              ),
              actions: (
                // we've added some custom button actions
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.Herramientas.find(o => o.id === prop.idherramienta);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                    icon="true"
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.Herramientas.find(o => o.id === prop.idherramienta);
                      this.onRemoveTool(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                    icon="true"
                  >
                    <i className="fa fa-trash" />
                  </Button>
                </div>
              )
            };
          });
          this.setState({ Herramientas });
        }
      }).catch(error => {
        this.setState({ blocking: false});
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
  }
 
  getFile(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file: { extencion, base64 } })
  }
  componentDidMount() {
    this.onLoadTools();
  }
  render() {
    const { newHerramienta,editTool,editarImg } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              {!newHerramienta && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <form id="toolsForm">
                          <BlockUi tag="div" blocking={this.state.blocking}>
                            <Row>
                              <Col md={12} className="mb-2">
                                {!editTool && (
                                  <h4 className="col-md-6 ml0 numbers text-danger media">Crear herramienta comercial</h4>
                                )}
                                {editTool && (
                                  <h4 className="col-md-6 ml0 numbers text-danger media">Editar herramienta comercial</h4>
                                )}
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12} className="pl25">
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Nombre</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={(e) => this.setState({ nombre: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Texto previo</ControlLabel>
                                    <FormControl placeholder="Escribe un texto" value={this.state.textoprevio} onChange={(e) => this.setState({ textoprevio: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            {!editTool && (
                              <Row>
                                <Col md={12} className="pl25">
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Imagen</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFile.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                </Col>
                              </Row>)}
                            <Row>
                              <Col md={12} className="pl15">
                                <Col md={12}>
                                  <FormGroup>
                                    <ControlLabel>Texto principal</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      value={this.state.textoprincipal} onChange={(e) => this.setState({ textoprincipal: e.target.value })}
                                      placeholder="Escribe texto principal"
                                      type="text"
                                      componentClass="textarea" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            {editTool && (
                              <Col md={8} className="mb-2">
                                <Col md={6}>
                                  {!editarImg && (
                                    <Thumbnail src={this.state.img} alt="242x200">
                                      <h3 className="text-center">Imagen</h3>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editar}
                                          bsStyle="primary"
                                          simple><i className="fa fa-edit"></i></Button>
                                      </p>
                                    </Thumbnail>
                                  )}
                                  {editarImg && (
                                    <div className="thumbnail">
                                      <h3 className="text-center">Imagen</h3>
                                      <div className="custom_file_upload mb-2">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getImg.bind(this)} />
                                        </div>

                                      </div>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editar}
                                          bsStyle="danger"
                                          simple><i className="fa fa-times fa-2x"></i></Button>
                                        <Button
                                          onClick={this.updateImage}
                                          bsStyle="success"
                                          simple><i className="fa fa-check fa-2x"></i></Button>
                                      </p>
                                    </div>
                                  )}
                                </Col>
                              </Col>
                            )}
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                          </Button>
                                </Col>
                                {!editTool && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                      Agregar
                                </Button>
                                  </Col>
                                )}

                                {editTool && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                      Guardar
                                </Button>
                                  </Col>
                                )}
                              </Col>
                            </Row>
                          </BlockUi>
                        </form>
                      }
                    />
                  </Col>
                </Row>)}
              {newHerramienta && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nuevo
                          </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.Herramientas}
                              filterable
                              columns={[
                                {
                                  Header: "Imagen",
                                  accessor: "image"
                                },
                                {
                                  Header: "Título/nombre",
                                  accessor: "name",
                                },
                                {
                                  Header: "Fecha creación",
                                  accessor: "fecha",
                                },

                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay información disponible'
                              pageText='Página'
                              ofText='de'
                              rowsText='filas'
                              defaultFilterMethod={this.filterMethod.bind(this)}
                              // Accessibility Labels
                              pageJumpText='ir a la página'
                              rowsSelectorText='filas por página'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
