
import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col } from "react-bootstrap";
import axios from 'axios';
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import 'react-block-ui/style.css';

class ReactTables extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [], logslogin: []
        };
        this.hideAlert = this.hideAlert.bind(this);
    }
    hideAlert() {
        this.setState({
            alert: null
        });
    }
    filterMethod = (filter, row, column) => {
        const id = filter.pivotId || filter.id;
        if (row[id] !== undefined) {
            if (String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    descargarCsv() {
        var url = "https://www.ideasqueactivanclaro.com.co/admin/logslogin/reporte/";
        var z = document.createElement("form");
        z.setAttribute('method', "get");
        z.setAttribute('action', url);
        z.setAttribute('id', "decargaReporte");

        z.setAttribute("target", "_blank");

        document.getElementById("formDescargarReporte").appendChild(z);


        z.submit();
        document.getElementById("decargaReporte").remove();
    }
    onloadLogslogin() {
        axios.get("https://www.ideasqueactivanclaro.com.co/admin/logslogin/")
            .then(res => {
                if (res.data.response) {
                    const logslogin = res.data.response.map((prop, key) => {
                        return {
                            correo: prop.correo,
                            dispositivo: prop.dispositivo,
                            fechaIngreso: prop.fechaIngreso
                        };
                    });
                    this.setState({ logslogin });
                }
            }).catch(error => {
                this.setState({ blocking: false });
                this.setState({
                    alert: (
                        <SweetAlert
                            style={{ display: "block", marginTop: "-100px" }}
                            onConfirm={() => this.hideAlert()}
                            onCancel={() => this.hideAlert()}
                            confirmBtnBsStyle="danger"
                            confirmBtnText="Aceptar"
                        >
                            No se recibió información. Inténtalo de nuevo.
            </SweetAlert>
                    )
                });
            });
    }
    componentDidMount() {
        this.onloadLogslogin();
    }
    render() {
        return (
            <div className="main-content">
                {this.state.alert}
                <Grid fluid>
                    <Row>
                        <Col md={12}>

                            <Row>
                                <Col md={12}>
                                    <Card
                                        title=""
                                        content={
                                            <Row>
                                                <div id="formDescargarReporte"></div>
                                                <Col md={12}>
                                                    <a onClick={this.descargarCsv.bind(this)} download="reporteLogs.csv" >
                                                        <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true">
                                                            Descargar CSV
                                                        </Button>
                                                    </a>
                                                </Col>
                                                <Col md={12} xs={12}>
                                                    <ReactTable
                                                        data={this.state.logslogin}
                                                        filterable
                                                        columns={[
                                                            {
                                                                Header: "Correo",
                                                                accessor: "correo"
                                                            },
                                                            {
                                                                Header: "Dispositivo",
                                                                accessor: "dispositivo",
                                                            },
                                                            {
                                                                Header: "Fecha de Ingreso",
                                                                accessor: "fechaIngreso",
                                                            }
                                                        ]}
                                                        defaultPageSize={10}
                                                        showPaginationTop={false}
                                                        previousText="Anterior"
                                                        nextText="Siguiente"
                                                        loadingText='Cargando...'
                                                        noDataText='No hay información disponible'
                                                        pageText="Pagina"
                                                        ofText='de'
                                                        rowsText='filas'
                                                        defaultFilterMethod={this.filterMethod.bind(this)}
                                                        // Accessibility Labels
                                                        pageJumpText='ir a la página'
                                                        rowsSelectorText='filas por página'
                                                        showPaginationBottom
                                                        className="-striped -highlight"
                                                    />
                                                </Col>
                                            </Row>
                                        }
                                    />
                                </Col>
                            </Row>

                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default ReactTables;
