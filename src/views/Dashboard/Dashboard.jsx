import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, Thumbnail, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';
import Select from "react-select";
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [], noticias: [], tipos: [{ "label": "1", "value": "1" },
      { "label": "2", "value": "2" },
      { "label": "3", "value": "3" },
      { "label": "4", "value": "4" },
      { "label": "5", "value": "5" }], 
      links: [{ "label": 'Inicio', "value": "inicio" },
      { "label": "Noticias", "value": "noticias" },
      { "label": "Campañas", "value": "campanas" },
      { "label": "Herramientas comerciales", "value": "herramientas_comerciales" },
      { "label": "Merchandising", "value": "merchandising" },
      { "label": "Concursos", "value": "concursos" },
      { "label": "Casas programadoras", "value": "casas_programadoras" },
      { "label": "Fabricantes", "value": "fabricantes" },
      { "label": "Novedades", "value": "novedades" },
      { "label": "Directorio proveedores", "value": "directorio_proveedores"}],
      news: false, preview: false, 
      editNew: false, 
      imgPreName: "", 
      filePre: {}, 
      imgPriName: "", 
      filePri: {}, 
      idbanner:"",
      editarImgPre: false, 
      editarImgPri: false, 
      imgName: "", 
      alert: null, 
      imgName2: "", 
      file1: {}, 
      file2: {}, 
      blocking: false, 
      nombre: "", 
      textoprevio: "", 
      textoprincipal: "", 
      img1: "", 
      img2: ""
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.togglePreview = this.togglePreview.bind(this);
    this.editarPre = this.editarPre.bind(this);
    this.editarPri = this.editarPri.bind(this);
    this.updateImgPre = this.updateImgPre.bind(this);
    this.updateImgPri = this.updateImgPri.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onSubmitUpdateForm = this.onSubmitUpdateForm.bind(this);
  }
  toggleBox() {
    const { news, editNew } = this.state;
    if (editNew === true) {
      this.setState({ editNew: false });
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "" ,linkSelect:null,singleSelect:null});
    }
    this.setState({ news: !news });
  }
  editarPre() {
    this.setState({ editarImgPre: !this.state.editarImgPre })
  }
  editarPri() {
    this.setState({ editarImgPri: !this.state.editarImgPri })
  }
  togglePreview() {
    this.setState({ preview: !this.state.preview })
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  filterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;
    if(row[id] !== undefined){
      if(String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }
  onSubmitUpdateForm() {
    const { nombre, seccion, singleSelect, idbanner } = this.state;
    if (nombre !== ""  && seccion !== "") {
      this.setState({ blocking: !this.state.blocking });
      var posicion=(singleSelect)?singleSelect.value:null;
      let dataSend = { data: { nombre, seccion, posicion, idbanner } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/banners/update", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadBanners();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", seccion: "", singleSelect: null });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  updateImg(file,tipo){
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { file,tipo ,idbanner:this.state.idbanner}
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/banners/updateImg", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onloadBanners(), 500);
            if(tipo==="previo"){
              this.setState({img1:file.base64});
              this.editarPre();
            }else{
              this.setState({img2:file.base64});
              this.editarPri();
            }
            
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }
        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });
  }
  updateImgPre() {
    if (this.state.filePre && this.state.filePre.base64) {
      this.updateImg(this.state.filePre,'previo');
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  updateImgPri() {
    if (this.state.filePri.base64) {
      this.updateImg(this.state.filePri,'principal');
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  onSubmitForm() {
    const { file1, file2, nombre, seccion, singleSelect } = this.state;
    if (file1.base64 && file2.base64 && nombre !== "" && seccion !== ""  ) {
      this.setState({ blocking: !this.state.blocking });
      var posicion=(singleSelect)?singleSelect.value:null;
      let dataSend = { data: { file1, file2, nombre, seccion, posicion } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/banners/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadBanners();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", seccion: "", singleSelect: null });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  getFile1(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file1: { extencion, base64 } })
  }
  getImgPre(files) {
    this.setState({ imgPreName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePre: { extencion, base64 } })
  }

  getImgPri(files) {
    this.setState({ imgPriName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePri: { extencion, base64 } })
  }

  getFile2(files) {
    this.setState({ imgName2: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file2: { extencion, base64 } })
  }
  setValues(data) {
    if (data) {
      const { nombre, seccion, imgApp, imgWeb, id,posicion } = data;
      var tipoAct,seccionAct;
      for(let a in this.state.tipos){
        if(this.state.tipos[a].value==posicion){
          tipoAct=this.state.tipos[a];
        }
      }
      for(let a in this.state.links){
        if(this.state.links[a].value===seccion){
          seccionAct=this.state.links[a];
        }
      }
      this.setState({ nombre, seccion, img1:imgApp, img2:imgWeb, idbanner: id ,linkSelect:seccionAct,singleSelect:tipoAct});
      this.setState({ editNew: !this.state.editNew });
      this.toggleBox();
    } else {
      this.setState({ nombre: "", seccion: "", img1: "", img2: "",singleSelect:null });
    }
  }
  onRemoveBanner(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteBanner(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  deleteBanner(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idbanner": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/banners/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onloadBanners(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onloadBanners() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/banners/", {headers: {"x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI="}})
      .then(res => {
        if (res.data.response) {
          const banners = res.data.response.map((prop, key) => {
            return {
              id: prop.idbanner,
              nombre:prop.nombre,
              imgApp: prop.imgApp,
              imgWeb: prop.imgWeb,
              posicion:prop.posicion,
              app:"SI",
              web:"SI",
              seccion: prop.seccion,
              fechaPublicacion:prop.fechaPublicacion,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.banners.find(o => o.id === prop.idbanner);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                  <Button
                    onClick={() => {
                      let obj = this.state.banners.find(o => o.id === prop.idbanner);
                      this.onRemoveBanner(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                </div>
              )
            };
          });
          this.setState({ banners });
      }
    }).catch(error => {
      this.setState({ blocking: false});
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
            </SweetAlert>
          )
        });
      });
  }
  componentDidMount() {
    this.onloadBanners();
  }
  render() {
    const { news, editNew, editarImgPre, editarImgPri } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              {news && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <form id="newsForm">
                          <BlockUi tag="div" blocking={this.state.blocking}>
                            <Row>
                              <Col md={10} className="mb-2">
                                {!editNew && (
                                  <h4 className="col-md-6 ml0 numbers text-danger media">Crear banner</h4>
                                )}
                                {editNew && (
                                  <h4 className="col-md-6 ml0 numbers text-danger media">Editar banner</h4>
                                )}
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                
                                <Col md={6}>
                                  <ControlLabel>Posición</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="singleSelect"
                                    value={this.state.singleSelect}
                                    options={this.state.tipos}
                                    onChange={(value) =>
                                      this.setState({ singleSelect: value })
                                    }
                                  />
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>nombre</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={(e) => this.setState({ nombre: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>

                            <Row>
                              <Col md={12}>
                              {!editNew && (
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Imagen Web</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName2} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFile2.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                )}
                                {!editNew && (
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Imagen app</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFile1.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>)}
                                  
                              </Col>

                            </Row>
                            <Row>
                              <Col md={12}>
                              <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Link</ControlLabel>
                                    <Select placeholder="Seleccionar"
                                      name="linkSelect"
                                      value={this.state.linkSelect}
                                      options={this.state.links}
                                      onChange={value =>
                                        this.setState({ linkSelect: value,seccion:(value)?value.value:"" })
                                      } />
                                  </FormGroup>
                                </Col>
                                
                              </Col>
                            </Row>
                            {editNew && (
                              <Row>
                                <Col md={8} className="mb-2">
                                  <Col md={6}>
                                    {!editarImgPre && (
                                    <Thumbnail src={this.state.img1} alt="242x200">
                                      <h3 className="text-center">Imagen App</h3>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editarPre}
                                          bsStyle="primary"
                                          simple><i className="fa fa-edit"></i></Button>
                                      </p>
                                    </Thumbnail>
                                    )}
                                    {editarImgPre && (
                                      <div className="thumbnail">
                                        <h3 className="text-center">Imagen App</h3>
                                        <div className="custom_file_upload mb-2">
                                          <input type="text" disabled="true" className="file" value={this.state.imgPreName} placeholder="Seleccione una imagen" name="file_info" />
                                          <div className="file_upload">
                                            <FileBase64
                                              multiple={false}
                                              onDone={this.getImgPre.bind(this)} />
                                          </div>

                                        </div>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPre}
                                            bsStyle="danger"
                                            simple><i className="fa fa-times fa-2x"></i></Button>
                                          <Button
                                            onClick={this.updateImgPre}
                                            bsStyle="success"
                                            simple><i className="fa fa-check fa-2x"></i></Button>
                                        </p>
                                      </div>
                                    )}
                                  </Col>
                                  <Col md={6}>
                                    {!editarImgPri && (
                                      <Thumbnail src={this.state.img2} alt="242x200">
                                        <h3 className="text-center">Imagen Web</h3>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPri}
                                            bsStyle="primary"
                                            simple><i className="fa fa-edit"></i></Button>
                                        </p>
                                      </Thumbnail>
                                    )}
                                    {editarImgPri && (
                                      <div className="thumbnail">
                                        <h3 className="text-center">Imagen Web</h3>
                                        <div className="custom_file_upload mb-2">
                                          <input type="text" disabled="true" className="file" value={this.state.imgPriName} placeholder="Seleccione una imagen" name="file_info" />
                                          <div className="file_upload">
                                            <FileBase64
                                              multiple={false}
                                              onDone={this.getImgPri.bind(this)} />
                                          </div>

                                        </div>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPri}
                                            bsStyle="danger"
                                            simple><i className="fa fa-times fa-2x"></i></Button>
                                          <Button
                                            onClick={this.updateImgPri}
                                            bsStyle="success"
                                            simple><i className="fa fa-check fa-2x"></i></Button>
                                        </p>
                                      </div>
                                    )}
                                  </Col>
                                </Col>
                              </Row>)}
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                                </Button>
                                </Col>
                                {!editNew && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                      Agregar
                                </Button>
                                  </Col>
                                )}

                                {editNew && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                      Guardar
                                </Button>
                                  </Col>
                                )}
                              </Col>
                            </Row>
                          </BlockUi>
                        </form>
                      }
                    />
                  </Col>
                </Row>)}
              {!news && (
                <Row>
                  <Col md={12}>
                    <Card
                      title=""
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nueva
                            </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.banners}
                              filterable
                              columns={[
                                {
                                  Header: "Posición",
                                  accessor: "posicion"
                                },
                                {
                                  Header: "Nombre",
                                  accessor: "nombre"
                                },
                                {
                                  Header: "App",
                                  accessor: "app",
                                },
                                
                                {
                                  Header: "Web",
                                  accessor: "web",
                                },
                                {
                                  Header: "Sección",
                                  accessor: "seccion",
                                },
                                {
                                  Header: "Creada",
                                  accessor: "fechaPublicacion",
                                },
                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay información disponible'
                              pageText="Pagina"
                              ofText='de'
                              rowsText='filas'
                              defaultFilterMethod={this.filterMethod.bind(this)}
                              // Accessibility Labels
                              pageJumpText='ir a la página'
                              rowsSelectorText='filas por página'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
