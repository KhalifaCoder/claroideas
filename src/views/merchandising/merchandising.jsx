import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, Nav, NavItem, Tab, Thumbnail, FormGroup, ControlLabel, FormControl, Modal } from "react-bootstrap";
import axios from 'axios';
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import Select from "react-select";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import ProductCard from 'components/UserCard/UserCard.jsx';
import Arrow from "assets/img/left-arrow.png";
import Checkbox from "components/CustomCheckbox/CustomCheckbox.jsx";

require('moment');
require('moment/locale/es');

class Merchandising extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enterprises: [],
      newEnterprice: false,
      persons: [],
      products: [],
      idPedido: "",
      infoProducts: [],
      newProfile: false,
      nombre: "",
      estados: [],
      details: [],
      blocking: false,
      apellidos: "",
      correo: "",
      stockProducts: [],
      showDist: false,
      isEnviado: false,
      isNovedad: false,
      idinventario: "",
      fechaNacimiento: "",
      hobbies: "",
      documento: "",
      editarImg: false,
      filesProduct: {},
      fijo: "",
      nombres: "",
      fechaSolicitud: "",
      estado: "",
      valor: "",
      showModal: false,
      totalArticulos: "",
      personacontacto: "",
      telefonocontacto: "",
      codigoD: "",
      ciudad: "",
      direccion: "",
      newProducts: false,
      editProduct: false,
      movil: "",
      alert: null,
      singleSelect: null,
      sucursalSelect: null,
      regionSelect: null,
      tipoDocSelect: null,
      imgName: "",
      files: [],
      filtroActivo:"",
      nombreSucursal: "",
      ciudadSucursal: "",
      direccionSucursal: "",
      correoSucursal: "",
      telefonoSucursal: "",
      razon: "",
      validator:false,
      valorProducto: "",
      nombreProducto: "",
      imgProductName: "",
      codigoProducto: "",
      productInventory: "",
      idDistribuidor: "",
      nit: "",
      distCounter:0,
      branchestemp: [],
      branches: [],
      tipos: [{ "label": "Cedula de ciudadania", "value": "1" }],
      regiones: [{ "label": "REGION 1", "value": "REGION 1" },
      { "label": "REGION 2", "value": "REGION 2" },
      { "label": "REGION 3", "value": "REGION 3" },
      { "label": "REGION 4", "value": "REGION 4" }]
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.toggleEnterprice = this.toggleEnterprice.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.toggleProducts = this.toggleProducts.bind(this);
    this.editar = this.editar.bind(this);
    this.updateImage = this.updateImage.bind(this);
    this.allocateInventory = this.allocateInventory.bind(this);
    this.validateProducts = this.validateProducts.bind(this);
    this.delegateProducts = this.delegateProducts.bind(this);
    this.verificarNumero=this.verificarNumero.bind(this);
    this.resetAllStock=this.resetAllStock.bind(this);
  }
  toggleBox() {
    const { newProfile } = this.state;
    this.setState({
      newProfile: !newProfile, singleSelect: null
    });
  }
  allocateInventory() {
    const elements = document.getElementsByClassName("inputProduct");
    var products = [];
    const { idDistribuidor } = this.state;
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].value !== "") {
        products.push({ idinventario: elements[i].id, cantidad: (Number(elements[i].value)), idinventarioXdist: elements[i].name });
      }
    }
    this.setState({ blocking: !this.state.blocking });
    let dataSend = { data: { idDistribuidor, products } };
    axios.post("https://www.ideasqueactivanclaro.com.co/admin/orders/allocateInventory", dataSend)
      .then(res => {
        var resp = res.data.response;
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error === 0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
          this.onLoadEnterprise();
          setTimeout(this.toggleEnterprice(), 100);
        } else {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                danger
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
        }

      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  verificarNumero(){

      let distCounter=document.querySelectorAll('input:checked');
      let {validator}=this.state;
      if(distCounter.length>0){
        if(distCounter.length===10){
          this.setState({distCounter:10});
          var unchecked=document.querySelectorAll('input[type="checkbox"]:not(:checked)');
          for (let index = 0; index < unchecked.length; index++) {
            unchecked[index].disabled=true;
          }
          this.setState({validator:true});
        }else{
          this.setState({distCounter:distCounter.length});
          if(validator){
            var unchecked=document.querySelectorAll('input[type="checkbox"]:not(:checked)');
            for (let index = 0; index < unchecked.length; index++) {
              unchecked[index].disabled=false;
            }
            this.setState({validator:false});
          }
        }
      }else{
        this.setState({distCounter:0});
      }
  }
  changeState(value) {
    this.setState({ singleSelect: value });
    if (value.label === "NOVEDAD") {
      this.setState({ isEnviado: false });
      this.setState({ isNovedad: true });
    } else if (value.label === "DESPACHADO") {
      this.setState({ isEnviado: true });
      this.setState({ isNovedad: false });
    } else {
      this.setState({ isEnviado: false });
      this.setState({ isNovedad: false });
    }
  }
  onLoadDetailsOrder(idPedido) {
    this.setState({ blocking: !this.state.blocking });
    axios.get(`https://www.ideasqueactivanclaro.com.co/admin/orders/detailsOrder?id=${idPedido}&token=U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=`)
      .then(res => {
        this.setState({ blocking: !this.state.blocking });
        if (res.data.response) {
          const details = res.data.response.detalle.map((prop) => {
            return {
              id: prop.idInventario,
              nombre: prop.nombre,
              codigo: prop.codigo,
              valorTotal: prop.valorTotal,
              valor: prop.valorUni,
              img: prop.img,
              cantidad: prop.cantidad,
              image: (
                <Col md={5}>
                  <Thumbnail src={prop.img} alt="242x200">
                  </Thumbnail>
                </Col>
              )
            };
          });
          const estados = res.data.response.estados.map((prop, key) => {
            return {
              label: prop.estado,
              value: prop.idEstadoPedido
            }
          })
          this.setState({ details, estados });
          this.toggleBox();
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  filterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;
    if (row[id] !== undefined) {
      if(Number.isInteger(parseInt(filter.value))){
        if (String(row[id]).startsWith(filter.value) || String(row[id]).includes(filter.value)) {
          return true;
        } else {
          return false;
        }
      }else{
        if (String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())) {
          return true;
        } else {
          return false;
        }
      }
      
    } else {
      return true;
    }
  }
  onRemoveOrder(id){
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteOrder(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  onRemoveProduct(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteProduct(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  deleteOrder(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idPedido": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/orders/deleteOrder", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onLoadDist(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  deleteProduct(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idinventario": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/orders/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onLoadBranchs(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  setValues(data) {
    if (data) {
      const { fechaSolicitud, estado, valor, totalArticulos, personacontacto, telefonocontacto, codigoD, ciudad, direccion, nombres, id } = data;
      this.setState({ fechaSolicitud, estado, valor, totalArticulos, personacontacto, telefonocontacto, codigoD, ciudad, direccion, nombres, idPedido: id });
      this.onLoadDetailsOrder(id);
    } else {
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "", img1: "", img2: "" });
    }
  }
  onLoadStockDist(obj) {
    this.setState({ blocking: !this.state.blocking });
    this.setState({ nombreDist: obj.nombre, idDistribuidor: obj.id });
    axios.get(`https://www.ideasqueactivanclaro.com.co/admin/orders/stock?id=${obj.id}`, { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        this.setState({ blocking: !this.state.blocking });
        if (res.data.response) {
          const infoProducts = res.data.response[0].map((prop) => {
            return {
              id: prop.idinventario,
              nombre: prop.nombre,
              codigo: prop.codigo,
              valor: prop.valor,
              img: prop.img,
              idinventarioXdist: prop.idinventarioXdist,
              cantidad: prop.cantidad,
              image: (
                <Col md={5}>
                  <Thumbnail src={prop.img} alt="242x200">
                  </Thumbnail>
                </Col>
              )
            };
          });
          this.setState({ infoProducts });
          this.toggleEnterprice();
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  resetAllStock() {
    this.setState({ blocking: !this.state.blocking });
    axios.get(`https://www.ideasqueactivanclaro.com.co/admin/orders/resetAllStock`, { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error==0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
               {res.data.response}
                </SweetAlert>
            )
          });
          setTimeout(this.onLoadDist(),500);
          setTimeout(this.onLoadEnterprise(),500);
          setTimeout(this.onLoadBranchs(),500);
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  setProductValues(data) {
    if (data) {
      const { nombre, codigo, valor, id, img } = data;
      this.setState({ nombreProducto: nombre, codigoProducto: codigo, valorProducto: valor, idinventario: id, img });
      this.setState({ editProduct: !this.state.editProduct });
      this.toggleProducts();
    }
  }
  onSubmitProductForm() {
    const { nombreProducto, codigoProducto, valorProducto, filesProduct } = this.state;
    if (nombreProducto !== "" && codigoProducto !== "" && valorProducto !== "" && filesProduct.base64) {
      let dataSend = { data: { nombreProducto, codigoProducto, valorProducto, filesProduct } };
      this.setState({ blocking: !this.state.blocking });
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/orders/product", dataSend)
        .then(res => {
          this.setState({ blocking: !this.state.blocking });
          var resp = res.data.response;
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.setState({ codigoProducto: "", nombreProducto: "", valorProducto: "", filesProduct: {}, imgProductName: "" });
            this.toggleProducts();
            this.onLoadBranchs();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }
        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  editar() {
    this.setState({ editarImg: !this.state.editarImg })
  }
  updateImage() {
    if (this.state.file && this.state.file.base64) {
      this.updateImg(this.state.file);
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  updateImg(file) {
    this.setState({ blocking: !this.state.blocking });
    let fields = {
      data: { file, idinventario: this.state.idinventario }
    };
    axios.post("https://www.ideasqueactivanclaro.com.co/admin/orders/updateImg", fields)
      .then(res => {
        var resp = res.data.response;
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error === 0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
          setTimeout(this.onLoadBranchs(), 500);
          this.setState({ img: file.base64 });
          this.editar();
        } else {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                danger
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"

              >
                {resp}
              </SweetAlert>
            )
          });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  getImg(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file: { extencion, base64 } })
  }
  onSubmitUdateProductForm() {
    const { nombreProducto, codigoProducto, valorProducto, idinventario } = this.state;
    if (nombreProducto !== "" && codigoProducto !== "" && valorProducto !== "") {
      let dataSend = { data: { nombreProducto, codigoProducto, valorProducto, idinventario } };
      this.setState({ blocking: !this.state.blocking });
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/orders/productUpdate", dataSend)
        .then(res => {
          this.setState({ blocking: !this.state.blocking });
          var resp = res.data.response;
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.setState({ codigoProducto: "", nombreProducto: "", valorProducto: "", filesProduct: {}, imgProductName: "" });
            this.toggleProducts();
            this.onLoadBranchs();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }
        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  validateProducts () {
    const elements = document.getElementsByClassName("inputStockProduct");
    var stockProducts = [];
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].value !== "") {
        stockProducts.push({ idinventario: elements[i].id, cantidad: (Number(elements[i].value)) });
      }
    }
    if (stockProducts.length > 0) {
      this.setState({stockProducts,showDist:true});
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            success
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="warning"
            confirmBtnText="Aceptar"
          >
            Debes asignar la cantidad de productos
          </SweetAlert>
        )
      });
    }
  }
  async delegateProducts() {
    let distribuidores=document.querySelectorAll('input:checked');
    var asignedProducts = [];
    if (distribuidores.length > 0) { 
      for (var a in distribuidores) {
        for (var e in this.state.stockProducts) {
          if (distribuidores[a].id !== undefined) {
            await asignedProducts.push({ idInventario: this.state.stockProducts[e].idinventario, cantidad: this.state.stockProducts[e].cantidad, idDistribuidor: distribuidores[a].id });
          }
        }
      }
      if(asignedProducts.length>0){
        this.setState({ showModal: false ,showDist:false,distCounter:0,
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              warning
              showCancel
              confirmBtnText="Aceptar"
              cancelBtnText="Cancelar"
              confirmBtnBsStyle="danger"
              cancelBtnBsStyle="default"
              title="¿Estas seguro?"
              onConfirm={() => this.sendAllAsignedProducts(asignedProducts)}
              onCancel={() => this.hideAlert()}
            >
              Recuerda que este Stock solo se aplicará a distribuidores en estado SIN STOCK.
        </SweetAlert>
          )
        });
      }
    }else{
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            success
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="success"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar por lo menos un distribuidor
          </SweetAlert>
        )
      });
    }
  }
  sendAllAsignedProducts(data){
    this.setState({ blocking: true});
    axios.post("https://www.ideasqueactivanclaro.com.co/admin/orders/allocateSelected", {data})
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: false});
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onLoadEnterprise();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });
  }
  onSubmitBranchesForm() {
    const { razon, nit, nombreSucursal, ciudadSucursal, direccionSucursal, correoSucursal, telefonoSucursal } = this.state;
    if (razon !== "" && nit !== "" && nombreSucursal !== "" && ciudadSucursal !== "" && direccionSucursal !== "" && correoSucursal !== "" && telefonoSucursal !== "") {
      this.setState({ blocking: !this.state.blocking });
      var principal = document.getElementById("principal").value;
      principal = (principal === true) ? 1 : 0;
      let dataSend = { data: { razon, principal, nit, nombreSucursal, ciudadSucursal, direccionSucursal, correoSucursal, telefonoSucursal } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/enterprises/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("branchesForm").reset();
            this.setState({ razon: "", nit: "", nombreSucursal: "", ciudadSucursal: "", direccionSucursal: "", telefonoSucursal: "", correoSucursal: "" });
            this.onLoadEnterprise();
            this.toggleEnterprice();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  toggleEnterprice() {
    this.setState({
      newEnterprice: !this.state.newEnterprice
    });
  };
  toggleProducts() {
    if (this.state.editProduct === true) {
      this.setState({ editProduct: false, nombreProducto: "", codigoProducto: "", valorProducto: "", img: "" })
    }
    this.setState({
      newProducts: !this.state.newProducts
    });
  };
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  getFiles(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ files: { extencion, base64 } })
  }
  getFilesProduct(files) {
    this.setState({ imgProductName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filesProduct: { extencion, base64 } })
  }
  onSubmitForm() {
    const { singleSelect, novedad, guia, idPedido } = this.state;
    if (singleSelect !== null) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          "estado": singleSelect.value,
          "novedad": (novedad) ? novedad : "",
          "guia": guia ? guia : "",
          idPedido
        }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/orders/updateStateOrder", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            }); this.onLoadDist();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }

  }

  onInputNameChange(e) {
    this.setState({ nombre: e.target.value });
  }
  onInputLastNameChange(e) {
    this.setState({ apellidos: e.target.value });
  }
  onInputEmailChange(e) {
    this.setState({ correo: e.target.value });
  }
  onInputDateChange(e) {
    this.setState({ fechaNacimiento: e.target.value });
  }
  onInputDNChange(e) {
    this.setState({ documento: e.target.value });
  }
  onInputFijoChange(e) {
    this.setState({ fijo: e.target.value });
  }
  onInputMovilChange(e) {
    this.setState({ movil: e.target.value });
  }
  onInputHobbiesChange(e) {
    this.setState({ hobbies: e.target.value });
  }
  loadDataBranches(id) {
    let branches = [];
    this.state.branchestemp.map((prop, key) => {
      if (prop.id === id.id) {
        branches.push(prop)
      }
      return key;
    });
    this.setState({ singleSelect: id })
    this.setState({ branches });
  }
  onLoadDist() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/orders/", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const orders = res.data.response.map((prop, key) => {
            return {
              id: prop.idPedido,
              nombres: prop.nombreDistribuidor,
              fechaSolicitud: prop.fechaSolicitud,
              estado: prop.estado,
              valor: prop.valorTotal,
              totalArticulos: prop.totalArticulos,
              personacontacto: prop.personacontacto,
              telefonocontacto: prop.telefonocontacto,
              codigoD: prop.codigoD,
              ciudad: prop.ciudad,
              direccion: prop.direccion,
              actions: (
                <div className="actions-right">
                  <Button
                    onClick={() => {
                      let obj = this.state.orders.find(o => o.id === prop.idPedido);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-eye" />
                  </Button>
                  <Button
                    onClick={() => {
                      let obj = this.state.orders.find(o => o.id === prop.idPedido);
                      this.onRemoveOrder(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>
                </div>
              )
            };
          });
          this.setState({ orders });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  onLoadEnterprise() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/orders/invetary", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const inventary = res.data.response.map((prop, key) => {
            return {
              id: prop.Distribuidor,
              nombre: prop.nombreDistribuidor,
              nombreEmpresa: prop.nombreEmpresa,
              stateStock: (prop.total === "1") ? "CON STOCK" : "SIN STOCK",
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.inventary.find(o => o.id === prop.Distribuidor);
                      this.onLoadStockDist(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ inventary });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  onLoadBranchs() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/orders/products", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const products = res.data.response.map((prop, key) => {
            return {
              id: prop.idinventario,
              nombre: prop.nombre,
              codigo: prop.codigo,
              valor: prop.valor,
              img: prop.img,
              image: (
                <Col md={5}>
                  <Thumbnail src={prop.img} alt="242x200">
                  </Thumbnail>
                </Col>
              ),
              actions: (
                // we've added some custom button actions
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.products.find(o => o.id === prop.idinventario);
                      this.setProductValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                    icon="true"
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.products.find(o => o.id === prop.idinventario);
                      this.onRemoveProduct(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                    icon="true"
                  >
                    <i className="fa fa-trash" />
                  </Button>
                </div>
              )
            };
          });
          this.setState({ products });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  descargarCsv() {
    var filtroActivo=document.getElementById("estadoSelector").value;
    var url="https://www.ideasqueactivanclaro.com.co/admin/document/?estado="+filtroActivo;
    var z = document.createElement("form");
    z.setAttribute('method', "get");
    z.setAttribute('action', url);
    z.setAttribute('id', "decargaReporte");

    z.setAttribute("target", "_blank");
    
    document.getElementById("formDescargarReporte").appendChild(z);
    

    z.submit();
    document.getElementById("decargaReporte").remove();
  }
  componentDidMount() {
    this.onLoadDist();
    this.onLoadEnterprise();
    this.onLoadBranchs();
  }
  render() {
    const { newProfile, isEnviado, isNovedad, newEnterprice, newProducts, editProduct, editarImg, showDist,distCounter } = this.state;
    const tabs = (
      <Tab.Container id="tabs-with-dropdown" defaultActiveKey="pedidos">
        <Row className="clearfix">
          <Col sm={12}>
            <Nav bsStyle="tabs">
              <NavItem eventKey="pedidos">Pedidos</NavItem>
              <NavItem eventKey="stock">Stock por distribuidor</NavItem>
              <NavItem eventKey="inventario">Inventario</NavItem>
            </Nav>
          </Col>
          <Col sm={12}>
            <Tab.Content animation>
              <Tab.Pane eventKey="pedidos">
                {!newProfile && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                    <div id="formDescargarReporte"></div>
                      <Col md={12}>
                        <a onClick={this.descargarCsv.bind(this)} download="reporte.xlsx" >
                          <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true">
                            Descargar CSV
                      </Button>
                        </a>
                      </Col>
                    </Row>
                  </BlockUi>
                )}
                {newProfile && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                      
                      <Col md={12}>
                        <h4 className="numbers text-danger">Resumen del pedido</h4>
                      </Col>
                      <Col md={12}>
                        <Card shadow="ns"
                          content={
                            <form id="distForm">
                              <Col md={12}>
                                <Col md={4} className="pl5">
                                  <FormGroup>
                                    <h6>Fecha de solicitud</h6>
                                    <p className="text-muted">{this.state.fechaSolicitud}</p>
                                  </FormGroup>
                                </Col>
                                <Col md={4} className="p0">
                                  <FormGroup>
                                    <h6>Nombre de distribuidor</h6>
                                    <p className="text-muted">{this.state.nombres}</p>
                                  </FormGroup>
                                </Col>
                                <Col md={4} className="p0">
                                  <FormGroup>
                                    <h6>Codigo D / Centro de costos</h6>
                                    <p className="text-muted">{this.state.codigoD}</p>
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <h6>Persona de contacto</h6>
                                    <p className="text-muted">{this.state.personacontacto}</p>
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <h6>Teléfono de contacto</h6>
                                    <p className="text-muted">{this.state.telefonocontacto}</p>
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <h6>Dirección de envío</h6>
                                    <p className="text-muted">{this.state.direccion}</p>
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <h6>Ciudad de envío</h6>
                                    <p className="text-muted">{this.state.ciudad}</p>
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup className="mb0">
                                    <p className="text-muted m0 fs15">Artículos solicitados</p>
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12} className="footer">
                                <hr />
                              </Col>
                              <Col md={12}>
                                <ProductCard data={this.state.details} />
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup className="mb0">
                                    <p className="text-muted m0 fs15">Costos</p>
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12} className="footer">
                                <hr />
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <Row>
                                    <Col md={6}>
                                      <FormGroup>
                                        <h4 className="fwbold mt10">Total artículos</h4>
                                      </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                      <FormGroup>
                                        <h4 className="fwbold mt10">{this.state.totalArticulos}</h4>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                </Col>
                                <Col md={6}>
                                  <Row>
                                    <Col md={6}>
                                      <Col md={12}>
                                        <FormGroup>
                                          <h4 className="fwbold mb0 mt10">Valor total</h4>
                                          <p className="text-muted fs14">No incluye IVA</p>
                                        </FormGroup>
                                      </Col>
                                    </Col>
                                    <Col md={6}>
                                      <FormGroup>
                                        <h4 className="fwbold text-danger mt10">{this.state.valor}</h4>
                                      </FormGroup>
                                    </Col>
                                  </Row>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup className="mb0">
                                    <p className="text-muted m0 fs15">Estado actual del pedido: <span className="text-danger">{this.state.estado}</span></p>
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12} className="footer">
                                <hr />
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Selecciona el estado del pedido</ControlLabel>
                                    <Select placeholder="Seleccionar"
                                      name="singleSelect"
                                      value={this.state.singleSelect}
                                      options={this.state.estados}
                                      onChange={(value) =>
                                        this.changeState(value)
                                      }
                                    />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12} className="mb-2">
                                {isNovedad && (
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Novedades</ControlLabel>
                                      <FormControl placeholder="Escribe la novedad" value={this.state.novedad} onChange={(e) => this.setState({ novedad: e.target.value })} type="text" />
                                    </FormGroup>
                                  </Col>
                                )}
                                {isEnviado && (
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Guía en entrega</ControlLabel>
                                      <FormControl placeholder="Escribe número de guia" value={this.state.guia} onChange={(e) => this.setState({ guia: e.target.value })} type="text" />
                                    </FormGroup>
                                  </Col>
                                )}
                              </Col>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                            </Button>
                                </Col>
                                <Col md={5}>
                                  <Button bsStyle="danger" type="button" onClick={this.onSubmitForm.bind(this)} bsSize="large" fill >
                                    Actualizar
                            </Button>
                                </Col>
                              </Col>
                            </form>
                          }
                        />
                      </Col>
                    </Row>
                  </BlockUi>)}
                {!newProfile && (
                  <Row>
                    <Col md={12}>
                      <Card
                        content={
                          <ReactTable
                            data={this.state.orders}
                            filterable
                            columns={[
                              {
                                Header: "Distribuidor",
                                accessor: "nombres"
                              },
                              {
                                Header: "Codigo",
                                accessor: "id"
                              },
                              {
                                Header: "Articulos",
                                accessor: "totalArticulos"
                              },
                              {
                                Header: "Creada",
                                accessor: "fechaSolicitud"
                              },
                              {
                                Header: "Estado",
                                accessor: "estado",
                                id:"estado",
                                filterMethod: (filter, row) => {
                                  if (filter.value == "todos") {
                                    return row;
                                  }
                                  return (row[filter.id] == filter.value)?row:false;
                                  
                                },
                                Filter: ({ filter, onChange }) =>
                                  <select
                                    onChange={(event) => {onChange(event.target.value)}}
                                    style={{ width: "100%" }}
                                    id="estadoSelector"
                                    value={filter ? filter.value : "todos"}
                                  >
                                    <option value="todos" selected>Selecciona</option>
                                    <option value="SOLICITADO" >SOLICITADO</option>
                                    <option value="EN TRAMITE" >TRAMITE</option>
                                    <option value="DESPACHADO" >DESPACHADO</option>
                                    <option value="NOVEDAD" >NOVEDAD</option>                                   
                                  </select>
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay información disponible'
                            pageText='Página'
                            ofText='de'
                            rowsText='filas'
                            defaultFilterMethod={this.filterMethod.bind(this)}
                            // Accessibility Labels
                            pageJumpText='ir a la página'
                            rowsSelectorText='filas por página'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        }
                      />
                    </Col>
                  </Row>
                )}
              </Tab.Pane>
              <Tab.Pane eventKey="stock">
                {newEnterprice && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                      <Col md={12} className="mt5">
                        <Col md={1} className="pr0 mt5" onClick={this.toggleEnterprice}>
                          <img src={Arrow} alt="Flecha" />
                        </Col>
                        <Col md={6} className="pl0 ml0">
                          <h4 className="col-md-12 ml0 pl0 numbers text-danger media">{this.state.nombreDist}</h4>
                        </Col>
                        <Col md={4}>
                          <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.allocateInventory}>
                            Guardar
                          </Button>
                        </Col>
                      </Col>
                      <Col md={12}>
                        <Card shadow="ns"
                          content={
                            <Row>
                              <div className="col-md-12" style={{display: "flex",flexWrap:"wrap"}}>
                                {
                                  this.state.infoProducts.map((prop, key) => {
                                    return (
                                      <div key={key} className="col-xs-12 col-sm-6 col-md-6 col-lg-4" style={{display: "flex",flexDirection: "column",justifyContent: "space-around"}}>
                                        <div className="card pt7p">
                                          <div className="image">
                                            {prop.image}
                                            <div className="col-xs-7">
                                              <div className="author">
                                                {prop.nombre}
                                              </div>
                                            </div>
                                            <div className="col-xs-7">
                                              <p className="category fwbold">Código</p>
                                              <p className="description">{prop.codigo}</p>
                                            </div>
                                            <div className="col-xs-7">
                                              <p className="category fwbold">Ingrese Cantidad</p>
                                              <input type="number" min="0" placeholder="0" name={prop.idinventarioXdist} id={prop.id} className="mb-2 form-control text-center inputProduct" />
                                              <div style={{ display: "none" }}>
                                                {setTimeout(() => {
                                                  if (document.getElementById(prop.id)) {
                                                    document.getElementById(prop.id).value = prop.cantidad
                                                  }
                                                }, 10)}
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  })}
                              </div>
                            </Row>

                          }
                        />

                      </Col>

                    </Row>
                  </BlockUi>)}
                {!newEnterprice && (
                  <Row>
                    <Col md={12}>
                      <Row>
                        <Col md={12}>
                          <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={() => this.setState({ showModal: true })}>
                            Agregar Stock
                      </Button>
                          <Button className="pull-right mb-2 btn-fill mr20" bsStyle="danger" fill wd="true" onClick={this.resetAllStock}>
                            Reiniciar Stock
                      </Button>
                        </Col>
                        <Col md={6}>
                          <Modal
                            show={this.state.showModal}
                            onHide={() => this.setState({ showModal: false,showDist:false,distCounter:0 })}
                          >
                            <Modal.Header closeButton>
                            {showDist && (
                              <Modal.Title>Selecciona los distribuidores<span class="pull-right mr20">{distCounter}/10</span></Modal.Title>
                            )}
                            {!showDist && (
                              <Modal.Title>Selecciona los productos</Modal.Title>
                            )}
                            </Modal.Header>
                            <Modal.Body className="forceScroll">
                            {showDist && (
                                <Row >
                                  {this.state.inventary.map((prop, key) => {
                                    if(prop.stateStock ==="SIN STOCK"){
                                    return (
                                      <Col md={12} >
                                        <Col md={6} className="">
                                          <div className="author">
                                            <p style={{fontSize:"13px"}} className="category fwbold">{prop.nombre}</p>
                                          </div>
                                        </Col>

                                        <Col md={5} className="p0">
                                          <div className="author">
                                            <p style={{fontSize:"13px"}} className="category fwbold">{prop.nombreEmpresa}</p>
                                          </div>
                                        </Col>
                                        <Col md={1} className="p0">
                                          <Checkbox
                                            mar="m0"
                                            onClick={this.verificarNumero}
                                            number={prop.id}
                                          />
                                        </Col>
                                        <Col md={12} className="footer">
                                          <hr className="mt5" />
                                        </Col>
                                      </Col>
                                    )
                                    }
                                  })}

                                </Row>
                                )}
                                {!showDist && (
                                <Row >
                                  {this.state.products.map((prop, key) => {
                                    return (
                                      <Col md={12}>
                                        <Col md={3}>
                                          <Thumbnail src={prop.img} alt="242x200">
                                          </Thumbnail>
                                        </Col>
                                        <Col md={5}>
                                          <div className="author">
                                            <p className="category fwbold">{prop.nombre}</p>
                                          </div>
                                          <Col md={2}>
                                            <p className="category fwbold">Código</p>
                                            <p className="description">{prop.codigo}</p>
                                          </Col>
                                        </Col>

                                        <Col md={4}>
                                          <p className="category fwbold">Cantidad</p>
                                          <input type="number" min="0" placeholder="0" name={prop.nombre} id={prop.id} className="mb-2 form-control text-center inputStockProduct" />
                                        </Col>
                                        <Col md={12} className="footer">
                                          <hr className="mt5" />
                                        </Col>
                                      </Col>)
                                  })}
                                </Row>
                                )}
                            </Modal.Body>
                            <Modal.Footer>
                              <Button
                                simple
                                onClick={() => this.setState({ showModal: false,showDist:false,distCounter:0 })}
                              >
                                Cancelar
                        </Button>
                        {!showDist && (
                                <Button
                                  bsStyle="danger"
                                  fill
                                  onClick={() => this.validateProducts()}
                                >
                                  Continuar
                        </Button>
                         )}
                              {showDist && (
                                <Button
                                  bsStyle="danger"
                                  fill
                                  onClick={() => this.delegateProducts()}
                                >
                                  Asignar
                        </Button>
                              )}
                            </Modal.Footer>
                          </Modal>
                        </Col>
                      </Row>
                      <Card
                        content={
                          <ReactTable
                            data={this.state.inventary}
                            filterable
                            columns={[
                              {
                                Header: "Distribuidor",
                                accessor: "nombre"
                              },
                              {
                                Header: "Empresa",
                                accessor: "nombreEmpresa"
                              },
                              {
                                Header: "Estado",
                                accessor: "stateStock"
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay información disponible'
                            pageText='Página'
                            ofText='de'
                            rowsText='filas'
                            defaultFilterMethod={this.filterMethod.bind(this)}
                            // Accessibility Labels
                            pageJumpText='ir a la página'
                            rowsSelectorText='filas por página'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        }
                      />
                    </Col>
                  </Row>)}
              </Tab.Pane>
              <Tab.Pane eventKey="inventario">
                {!newProducts && (
                  <Row>
                    <Col md={12}>
                      <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleProducts}>
                        Crear nuevo
                      </Button>
                    </Col>
                  </Row>
                )}
                {newProducts && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                      <Col md={12}>
                        {!editProduct && (
                          <h4 className="numbers text-danger">Agregar nuevo articulo</h4>
                        )}
                        {editProduct && (
                          <h4 className="numbers text-danger">Editar articulo</h4>
                        )}
                      </Col>

                      <Col md={12}>
                        <Card shadow="ns"
                          content={
                            <form id="distForm">
                              <Col md={12}>
                                <Col md={6} className="pl5">
                                  <FormGroup>
                                    <ControlLabel>Nombre</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombreProducto} onChange={(e) => this.setState({ nombreProducto: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Código</ControlLabel>
                                    <FormControl placeholder="Escribe un código" value={this.state.codigoProducto} onChange={(e) => this.setState({ codigoProducto: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                {!editProduct && (
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Foto</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgProductName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFilesProduct.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                )}
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Valor unitario sin IVA</ControlLabel>
                                    <FormControl placeholder="Escribe el valor" value={this.state.valorProducto} onChange={(e) => this.setState({ valorProducto: e.target.value })} type="number" />
                                  </FormGroup>
                                </Col>
                              </Col>

                              {editProduct && (
                                <Col md={8} className="mb-2">
                                  <Col md={6}>
                                    {!editarImg && (
                                      <Thumbnail src={this.state.img} alt="242x200">
                                        <h3 className="text-center">Imagen</h3>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editar}
                                            bsStyle="primary"
                                            simple><i className="fa fa-edit"></i></Button>
                                        </p>
                                      </Thumbnail>
                                    )}
                                    {editarImg && (
                                      <div className="thumbnail">
                                        <h3 className="text-center">Imagen</h3>
                                        <div className="custom_file_upload mb-2">
                                          <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                          <div className="file_upload">
                                            <FileBase64
                                              multiple={false}
                                              onDone={this.getImg.bind(this)} />
                                          </div>

                                        </div>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editar}
                                            bsStyle="danger"
                                            simple><i className="fa fa-times fa-2x"></i></Button>
                                          <Button
                                            onClick={this.updateImage}
                                            bsStyle="success"
                                            simple><i className="fa fa-check fa-2x"></i></Button>
                                        </p>
                                      </div>
                                    )}
                                  </Col>
                                </Col>
                              )}
                              <Col md={12} className="mt3p">
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleProducts}>
                                    Cancelar
                            </Button>
                                </Col>

                                <Col md={5}>
                                  {!editProduct && (
                                    <Button bsStyle="danger" type="button" onClick={this.onSubmitProductForm.bind(this)} bsSize="large" fill >
                                      Agregar
                                    </Button>
                                  )}
                                  {editProduct && (
                                    <Button bsStyle="danger" type="button" onClick={this.onSubmitUdateProductForm.bind(this)} bsSize="large" fill >
                                      Guardar
                                    </Button>
                                  )}
                                </Col>
                              </Col>
                            </form>
                          }
                        />

                      </Col>

                    </Row>
                  </BlockUi>)}
                {!newProducts && (
                  <Row>
                    <Col md={12}>
                      <Card
                        content={
                          <ReactTable
                            data={this.state.products}
                            filterable
                            columns={[
                              {
                                Header: "Imagen",
                                accessor: "image"
                              },
                              {
                                Header: "Título/Nombre",
                                accessor: "nombre"
                              },
                              {
                                Header: "Código",
                                accessor: "codigo"
                              },
                              {
                                Header: "Valor unitario COP",
                                accessor: "valor"
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay información disponible'
                            pageText='Página'
                            ofText='de'
                            rowsText='filas'
                            defaultFilterMethod={this.filterMethod.bind(this)}
                            // Accessibility Labels
                            pageJumpText='ir a la página'
                            rowsSelectorText='filas por página'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        }
                      />
                    </Col>
                  </Row>
                )}
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    );
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card content={tabs} />
            </Col>
          </Row>

        </Grid>
      </div>
    );
  }
}

export default Merchandising;
