import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import FileBase64 from 'react-file-base64';
import SweetAlert from "react-bootstrap-sweetalert";
import Button from "components/CustomButton/CustomButton.jsx";
import BlockUi from 'react-block-ui';
import axios from 'axios';


class pdfCompetencia extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imgName: "",
            file: {},
            alert: null,
            showPdf: false
        };
        this.hideAlert = this.hideAlert.bind(this);
    }
    hideAlert() {
        this.setState({
            alert: null
        });
    }
    guardarPdf() {
        if (this.state.file.base64) {
            this.setState({ blocking: true });
            let dataSend = { data: { file:this.state.file} };
            axios.post("https://www.ideasqueactivanclaro.com.co/admin/crearPdf/", dataSend)
                .then(res => {
                    var resp = res.data.response;
                    this.setState({ blocking: !this.state.blocking });
                    if (res.data.error === 0) {
                        this.setState({ imgName: "" });
                        this.setState({ file: {} })
                        this.setState({ showPdf: false })
                        this.setState({
                            alert: (
                                <SweetAlert
                                    style={{ display: "block", marginTop: "-100px" }}
                                    success
                                    onConfirm={() => this.hideAlert()}
                                    onCancel={() => this.hideAlert()}
                                    confirmBtnBsStyle="success"
                                    confirmBtnText="Aceptar"
                                >
                                    {resp}
                                </SweetAlert>
                            )
                        }); 
                    } else {
                        this.setState({
                            alert: (
                                <SweetAlert
                                    style={{ display: "block", marginTop: "-100px" }}
                                    danger
                                    onConfirm={() => this.hideAlert()}
                                    onCancel={() => this.hideAlert()}
                                    confirmBtnBsStyle="danger"
                                    confirmBtnText="Aceptar"

                                >
                                    {resp}
                                </SweetAlert>
                            )
                        });
                    }

                }).catch(error => {
                    this.setState({ blocking: false });
                    this.setState({
                        alert: (
                            <SweetAlert
                                style={{ display: "block", marginTop: "-100px" }}
                                onConfirm={() => this.hideAlert()}
                                onCancel={() => this.hideAlert()}
                                confirmBtnBsStyle="danger"
                                confirmBtnText="Aceptar"
                            >
                                No se recibió información. Inténtalo de nuevo.
                            </SweetAlert>
                        )
                    });
                });
        } else {
            this.setState({ showPdf: false })
            this.setState({
                alert: (
                    <SweetAlert
                        style={{ display: "block", marginTop: "-100px" }}
                        danger
                        onConfirm={() => this.hideAlert()}
                        onCancel={() => this.hideAlert()}
                        confirmBtnBsStyle="danger"
                        confirmBtnText="Aceptar"
                    >
                        Debes seleccionar un archivo valido
                  </SweetAlert>
                )
            });
        }

    }
    getFile(files) {
        this.setState({ blocking: true });
        if (files.type == "application/pdf") {
            this.setState({ blocking: false });
            this.setState({ imgName: files.name });
            let extencion = files.name.split('.').pop();
            let base64 = files.base64;
            this.setState({ file: { extencion, base64 } })
            this.setState({ showPdf: true })

        } else {
            this.setState({ blocking: false });
            this.setState({ imgName: "" });
            this.setState({ file: {} })
            this.setState({ showPdf: false })
            this.setState({
                alert: (
                    <SweetAlert
                        style={{ display: "block", marginTop: "-100px" }}
                        danger
                        onConfirm={() => this.hideAlert()}
                        onCancel={() => this.hideAlert()}
                        confirmBtnBsStyle="danger"
                        confirmBtnText="Aceptar"
                    >
                        El archivo seleccionado no tiene el formato valido
              </SweetAlert>
                )
            });
        }


    }
    render() {
        const { showPdf } = this.state;
        return (
            <div className="content">
                {this.state.alert}
                <Grid fluid>
                    <BlockUi tag="div" blocking={this.state.blocking}>
                        <div className="card">
                            <div className="content">
                                <div className="places-buttons">
                                    <Row className="mt3p mb-2">
                                        <Col md={8} mdOffset={2} className="text-center">
                                            <h3 className="fwbold">
                                                Selecciona el pdf que deseas aplicar</h3>
                                        </Col>
                                    </Row>
                                    <Row className="mt3p mb-2">
                                        <Col md={5} mdOffset={3} className="text-center p0">
                                            <div className="custom_file_upload">
                                                <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione un archivo" name="file_info" />
                                                <div className="file_upload">
                                                    <FileBase64
                                                        multiple={false}
                                                        onDone={this.getFile.bind(this)} />
                                                </div>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt3p mb-2">
                                        <Col md={4} mdOffset={4} className="text-center p0">
                                            <Button bsStyle="danger" bsSize="large" fill onClick={this.guardarPdf.bind(this)}>
                                                Guardar
                                        </Button>
                                        </Col>
                                    </Row>
                                    {showPdf && (
                                        <Row className="mt3p mb-2">
                                            <Col md={8} mdOffset={2} className="text-center">
                                                <iframe src={this.state.file.base64} title="Competencias" style={{ width: "100%", height: "450px" }}>
                                                </iframe>
                                            </Col>
                                        </Row>
                                    )}

                                </div>
                            </div>
                        </div>
                    </BlockUi>
                </Grid>
            </div>
        );
    }
}

export default pdfCompetencia;
