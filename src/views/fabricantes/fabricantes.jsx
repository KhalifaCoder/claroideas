import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, Thumbnail, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], fabricantes: [],
      idfabricante:"", newFabricante: false, 
      editFabricante: false, imgName: "", alert: null,
       imgName2: "", file1: {}, file2: {}, blocking: false, 
       nombre: "", textoprevio: "", textoprincipal: "", img1: "",
        img2: "",imgPreName: "", 
        filePre: {}, 
        imgPriName: "", 
        filePri: {}, 
        editarImgPre: false, 
        editarImgPri: false,  };
    this.toggleBox = this.toggleBox.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.onSubmitUpdateForm = this.onSubmitUpdateForm.bind(this);
    this.editarPre = this.editarPre.bind(this);
    this.editarPri = this.editarPri.bind(this);
    this.updateImgPre = this.updateImgPre.bind(this);
    this.updateImgPri = this.updateImgPri.bind(this);

  }
  toggleBox() {
    const { newFabricante, editFabricante } = this.state;
    if (editFabricante === true) {
      this.setState({ editFabricante: false });
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "" });
    }
    this.setState({ newFabricante: !newFabricante });
  }
  editarPre() {
    this.setState({ editarImgPre: !this.state.editarImgPre })
  }
  filterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;
    if(row[id] !== undefined){
      if(String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }
  editarPri() {
    this.setState({ editarImgPri: !this.state.editarImgPri })
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  updateImg(file,tipo){
    this.setState({ blocking: !this.state.blocking });
    let fields = {
      data: { file,tipo ,idfabricante:this.state.idfabricante}
    };
    axios.post("https://www.ideasqueactivanclaro.com.co/admin/manufacturer/updateImg", fields)
      .then(res => {
        var resp = res.data.response;
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error === 0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
          setTimeout(this.onloadManofactures(), 500);
          if(tipo==="previo"){
            this.setState({img1:file.base64});
            this.editarPre();
          }else{
            this.setState({img2:file.base64});
            this.editarPri();
          }
          
        } else {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                danger
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"

              >
                {resp}
              </SweetAlert>
            )
          });
        }
      }).catch(error => {
        this.setState({ blocking: false});
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
}
updateImgPre() {
  if (this.state.filePre && this.state.filePre.base64) {
    this.updateImg(this.state.filePre,'previo');
  } else {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="danger"
          confirmBtnText="Aceptar"
        >
          Debes seleccionar una imagen.
        </SweetAlert>
      )
    });
  }
}
updateImgPri() {
  if (this.state.filePri.base64) {
    this.updateImg(this.state.filePri,'principal');
  } else {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="danger"
          confirmBtnText="Aceptar"
        >
          Debes seleccionar una imagen.
        </SweetAlert>
      )
    });
  }
}
  onSubmitUpdateForm() {
    const { nombre, textoprevio, textoprincipal, idfabricante } = this.state;
    if (nombre !== "" && textoprevio !== "" && textoprincipal !== "" ) {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { nombre, textoprevio, textoprincipal, idfabricante } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/manufacturer/update", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadManofactures();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("manofacturesForm").reset();
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", textoprevio: "", textoprincipal: "" });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  onSubmitForm() {
    const { file1, file2, nombre, textoprevio, textoprincipal } = this.state;
    if (file1 !== "" && file2 !== "" && nombre !== "" && textoprevio !== "" && textoprincipal !== "") {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { file1, file2, nombre, textoprevio, textoprincipal } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/manufacturer/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadManofactures();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("manofacturesForm").reset();
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", textoprevio: "", textoprincipal: "" });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  getImgPre(files) {
    this.setState({ imgPreName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePre: { extencion, base64 } })
  }

  getImgPri(files) {
    this.setState({ imgPriName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePri: { extencion, base64 } })
  }
  getFile1(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file1: { extencion, base64 } });
  }

  getFile2(files) {
    this.setState({ imgName2: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file2: { extencion, base64 } });
  }
  setValues(data) {
    if (data.nombre) {
      const { nombre, textoprevio, textoprincipal, img1, img2,id } = data;
      this.setState({ nombre, textoprevio, textoprincipal, img1, img2 ,idfabricante:id});
      this.setState({ editFabricante: !this.state.editFabricante });
      this.toggleBox();
    } else {
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "" });
    }
  }

  onRemoveManufacturer(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteManufacturer(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }

  deleteManufacturer(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idfabricante": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/manufacturer/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onloadManofactures();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onloadManofactures() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/manufacturer/", {headers: {"x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI="}})
      .then(res => {
        if (res.data.response) {
          const fabricantes = res.data.response.map((prop, key) => {
            return {
              id: prop.idfabricante,
              nombre: prop.nombre,
              textoprevio: prop.textoprevio,
              textoprincipal: prop.textoprincipal,
              img1: prop.imgprevia,
              img2: prop.imgprincipal,
              datecreation: prop.fechaCreacion,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.fabricantes.find(o => o.id === prop.idfabricante);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.fabricantes.find(o => o.id === prop.idfabricante);
                      this.onRemoveManufacturer(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });

          this.setState({ fabricantes });
        }

      }).catch(error => {
        this.setState({ blocking: false});
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
  }
  componentDidMount() {
    this.onloadManofactures();
  }
  render() {
    const { newFabricante, editFabricante,editarImgPre,editarImgPri } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              {newFabricante && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <form id="manofacturesForm">
                          <BlockUi tag="div" blocking={this.state.blocking}>
                            {!editFabricante && (
                              <Row>
                                <Col md={12} className="mb-2">
                                  <h4 className="numbers text-danger media">Crear fabricante</h4>
                                </Col>
                              </Row>)}
                            {editFabricante && (
                              <Row>
                                <Col md={12} className="mb-2">
                                  <h4 className="numbers text-danger media">Editar fabricante</h4>
                                </Col>
                              </Row>)}
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Título/ nombre de fabricante</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={(e) => this.setState({ nombre: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Texto previo</ControlLabel>
                                    <FormControl placeholder="Escribe un texto" value={this.state.textoprevio} onChange={(e) => this.setState({ textoprevio: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            {!editFabricante && (
                              <Row>
                                <Col md={12}>
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Imagen principal</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFile1.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Imagen previsualización</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName2} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFile2.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                </Col>
                              </Row>)}
                            <Row>
                              <Col md={12} className="mb-2">
                                <Col md={12}>
                                  <FormGroup>
                                    <ControlLabel>Texto principal</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      placeholder="Texto principal"
                                      type="text"
                                      value={this.state.textoprincipal} onChange={(e) => this.setState({ textoprincipal: e.target.value })}
                                      componentClass="textarea" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            {editFabricante && (
                              <Row>
                              <Col md={8} className="mb-2">
                                <Col md={6}>
                                  {!editarImgPre && (
                                  <Thumbnail src={this.state.img1} alt="242x200">
                                    <h3 className="text-center">Imagen previsualización</h3>
                                    <p className="text-center">
                                      <Button
                                        onClick={this.editarPre}
                                        bsStyle="primary"
                                        simple><i className="fa fa-edit"></i></Button>
                                    </p>
                                  </Thumbnail>
                                  )}
                                  {editarImgPre && (
                                    <div className="thumbnail">
                                      <h3 className="text-center">Imagen previsualización</h3>
                                      <div className="custom_file_upload mb-2">
                                        <input type="text" disabled="true" className="file" value={this.state.imgPreName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getImgPre.bind(this)} />
                                        </div>

                                      </div>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editarPre}
                                          bsStyle="danger"
                                          simple><i className="fa fa-times fa-2x"></i></Button>
                                        <Button
                                          onClick={this.updateImgPre}
                                          bsStyle="success"
                                          simple><i className="fa fa-check fa-2x"></i></Button>
                                      </p>
                                    </div>
                                  )}
                                </Col>
                                <Col md={6}>
                                  {!editarImgPri && (
                                    <Thumbnail src={this.state.img2} alt="242x200">
                                      <h3 className="text-center">Imagen principal</h3>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editarPri}
                                          bsStyle="primary"
                                          simple><i className="fa fa-edit"></i></Button>
                                      </p>
                                    </Thumbnail>
                                  )}
                                  {editarImgPri && (
                                    <div className="thumbnail">
                                      <h3 className="text-center">Imagen principal</h3>
                                      <div className="custom_file_upload mb-2">
                                        <input type="text" disabled="true" className="file" value={this.state.imgPriName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getImgPri.bind(this)} />
                                        </div>

                                      </div>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editarPri}
                                          bsStyle="danger"
                                          simple><i className="fa fa-times fa-2x"></i></Button>
                                        <Button
                                          onClick={this.updateImgPri}
                                          bsStyle="success"
                                          simple><i className="fa fa-check fa-2x"></i></Button>
                                      </p>
                                    </div>
                                  )}
                                </Col>
                              </Col>
                            </Row>)}
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                                </Button>
                                </Col>
                                {!editFabricante && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                      Agregar
                                </Button>
                                  </Col>
                                )}

                                {editFabricante && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                      Guardar
                                </Button>
                                  </Col>
                                )}
                              </Col>
                            </Row>
                          </BlockUi>
                        </form>
                      }
                    />
                  </Col>
                </Row>)}
              {!newFabricante && (
                <Row>
                  <Col md={12}>
                    <Card
                      title=""
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nuevo
                            </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.fabricantes}
                              filterable
                              columns={[
                                {
                                  Header: "Nombre",
                                  accessor: "nombre"
                                },
                                {
                                  Header: "Fecha publicación",
                                  accessor: "datecreation",
                                },
                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay información disponible'
                              pageText="Pagina"
                              ofText='de'
                              rowsText='filas'
                              defaultFilterMethod={this.filterMethod.bind(this)}
                              // Accessibility Labels
                              pageJumpText='ir a la página'
                              rowsSelectorText='filas por página'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
