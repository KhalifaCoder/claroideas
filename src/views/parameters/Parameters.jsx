import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, Nav, NavItem, Tab, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';
import Select from "react-select";
import Datetime from "react-datetime";
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import Checkbox from "components/CustomCheckbox/CustomCheckbox.jsx";

require('moment');
require('moment/locale/es');

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enterprises: [],
      newEnterprice: false,
      persons: [],
      newProfile: false,
      nombre: "",
      blocking: false,
      apellidos: "",
      correo: "",
      fechaNacimiento: "",
      hobbies: "",
      documento: "",
      fijo: "",
      movil: "",
      editNew: false,
      alert: null,
      singleSelect: null,
      sucursalSelect: null,
      regionSelect: null,
      tipoDocSelect: null,
      imgName: "",
      files: [],
      branchesList: [],
      editEnterprise: false,
      nombreSucursal: "",
      ciudadSucursal: "",
      direccionSucursal: "",
      correoSucursal: "",
      telefonoSucursal: "",
      razon: "",
      nit: "",
      branchestemp: [],
      branches: [],
      tipos: [{ "label": "Cédula de Ciudadanía", "value": "1" }],
      regiones: [{ "label": "REGION 1", "value": "REGION 1" },
      { "label": "REGION 2", "value": "REGION 2" },
      { "label": "REGION 3", "value": "REGION 3" },
      { "label": "REGION 4", "value": "REGION 4" },
      { "label": "GERENCIA", "value": "GERENCIA" },
      { "label": "DIRECCIÓN", "value": "DIRECCIÓN" },
      ]
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.toggleEnterprice = this.toggleEnterprice.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.onSubmitUpdateForm = this.onSubmitUpdateForm.bind(this);
  }
  toggleBox() {
    const { newProfile, editNew } = this.state;
    if (editNew === true) {
      this.setState({ editNew: false });
      this.setState({ nombre: "", apellidos: "", documento: "", fechaNacimiento: "", movil: "", idperfil: "", fijo: "", correo: "", hobbies: "", sucursalSelect: null, singleSelect: null, regionSelect: null, tipoDocSelect: null });
    }
    this.setState({
      newProfile: !newProfile
    });
  }
  filterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;
    if(row[id] !== undefined){
      if(String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }
  onSubmitBranchesForm() {
    const { razon, nit, nombreSucursal, ciudadSucursal, direccionSucursal, correoSucursal, telefonoSucursal } = this.state;
    if (razon !== "" && nit !== "" && nombreSucursal !== "" && ciudadSucursal !== "" && direccionSucursal !== "" && correoSucursal !== "" && telefonoSucursal !== "") {
      this.setState({ blocking: !this.state.blocking });
      var principal = document.getElementById("principal").value;
      principal = (principal === true) ? 1 : 0;
      let dataSend = { data: { razon, principal, nit, nombreSucursal, ciudadSucursal, direccionSucursal, correoSucursal, telefonoSucursal } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/enterprises/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("branchesForm").reset();
            this.setState({ razon: "", nit: "", nombreSucursal: "", ciudadSucursal: "", direccionSucursal: "", telefonoSucursal: "", correoSucursal: "" });
            this.onLoadEnterprise();
            this.onLoadBranchs();
            this.toggleEnterprice();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  onSubmitUpdateBranchesForm() {
    const { razon, nit, nombreSucursal, ciudadSucursal, direccionSucursal, correoSucursal, telefonoSucursal,idSucursal,idEmpresa } = this.state;
    if (razon !== "" && nit !== "" && nombreSucursal !== "" && ciudadSucursal !== "" && direccionSucursal !== "" && correoSucursal !== "" && telefonoSucursal !== "") {
      this.setState({ blocking: !this.state.blocking });
      let dataSend = { data: { razon, nit, nombreSucursal, ciudadSucursal, direccionSucursal, correoSucursal, telefonoSucursal,idSucursal,idEmpresa}};
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/enterprises/update", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("branchesForm").reset();
            this.setState({ razon: "", nit: "", nombreSucursal: "", ciudadSucursal: "", direccionSucursal: "", telefonoSucursal: "", correoSucursal: "" });
            this.onLoadEnterprise();
            this.onLoadBranchs();
            this.toggleEnterprice();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  onRemoveDist(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteDist(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  deleteDist(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idDistribuidor": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/dist/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onLoadDist(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onRemoveEnterprises(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteEnterprises(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  deleteEnterprises(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idEmpresa": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/enterprises/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onLoadEnterprise(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  toggleEnterprice() {
    if (this.state.editEnterprise === true) {
      this.setState({ editEnterprise: false });
      this.setState({ razon: "", nit: "", branchesList: [], nombreSucursal: "", ciudadSucursal: "", direccionSucursal: "", telefonoSucursal: "", correoSucursal: "" });
    }
    this.setState({
      newEnterprice: !this.state.newEnterprice
    });
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  getFiles(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ files: { extencion, base64 } })
  }
  setEnterpriseValues(data) {
    if (data) {
      const { nombre, NIT, id } = data;
      this.setState({ editEnterprise: !this.state.editEnterprise });
      this.setState({ razon: nombre, nit: NIT,idEmpresa:id});
      this.toggleEnterprice();
      for (let a in this.state.branchestemp) {
        if (this.state.branchestemp[a].id === id) {
          this.setState({ nombreSucursal: this.state.branchestemp[a].nombre, ciudadSucursal: this.state.branchestemp[a].ciudad, direccionSucursal: this.state.branchestemp[a].direccion, telefonoSucursal: this.state.branchestemp[a].telefono, correoSucursal: this.state.branchestemp[a].correo,idSucursal:this.state.branchestemp[a].idSucursal });
          break;
        }
      }
    }
  }
  setValues(data) {
    if (data) {
      const { nombre, apellidos, documento, region, empresa, sucursal, TipoDocumento, hobbies, fechaNacimiento, movil, id, fijo, correo } = data;
      this.toggleBox();
      var documentType, regionAct, enterpriseAct, branchAct;
      for (let a in this.state.tipos) {
        if (this.state.tipos[a].label === TipoDocumento) {
          documentType = this.state.tipos[a];
        }
      }
      for (let a in this.state.regiones) {
        if (this.state.regiones[a].label === region) {
          regionAct = this.state.regiones[a];
        }
      }
      for (let a in this.state.enterprises) {
        if (this.state.enterprises[a].label === empresa) {
          enterpriseAct = this.state.enterprises[a];
        }
      }
      for (let a in this.state.branchestemp) {
        if (this.state.branchestemp[a].label === sucursal) {
          branchAct = this.state.branchestemp[a];
        }
      }
      this.setState({ nombre, apellidos, documento, sucursalSelect: branchAct, singleSelect: enterpriseAct, TipoDocumento, tipoDocSelect: documentType, regionSelect: regionAct, hobbies, movil, fechaNacimiento, idperfil: id, fijo, correo });
      this.setState({ editNew: !this.state.editNew });
    } else {
      this.setState({ nombre: "", apellidos: "", documento: "", fechaNacimiento: "", movil: "", idperfil: "", fijo: "", correo: "" });
    }
  }
  onSubmitForm(e) {
    var dataSend = this.state;
    dataSend.fechaNacimiento = document.getElementById("fechaNacimiento").value;
    if (dataSend.nombre !== "" && dataSend.apellidos !== "" && dataSend.tipoDocSelect !== null
      && dataSend.documento !== "" && dataSend.fechaNacimiento !== "" && dataSend.movil !== ""
      && dataSend.fijo !== "" && dataSend.correo !== "" && dataSend.singleSelect !== null
      && dataSend.sucursalSelect !== null && dataSend.regionSelect !== null) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          "nombres": dataSend.nombre,
          "apellidos": dataSend.apellidos,
          "idTipoDocumento": dataSend.tipoDocSelect.value,
          "numeroDocumento": dataSend.documento,
          "fechaNacimiento": dataSend.fechaNacimiento,
          "movil": dataSend.movil,
          "fijo": dataSend.fijo,
          "foto": dataSend.files,
          "correo": dataSend.correo,
          "idEmpresa": dataSend.singleSelect.value,
          "idSucursal": dataSend.sucursalSelect.value,
          "hobbies": dataSend.hobbies,
          "region": dataSend.regionSelect.value
        }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/dist/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("distForm").reset();
            this.setState({ nombre: "", apellidos: "", tipoDocSelect: null, documento: "", movil: "", fijo: "", correo: "", singleSelect: null, sucursalSelect: null, imgName: "", regionSelect: null, hobbies: "" });
            this.onLoadDist();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }

  }
  onSubmitUpdateForm(e) {
    var dataSend = this.state;
    if (dataSend.nombre !== "" && dataSend.apellidos !== "" && dataSend.tipoDocSelect !== null
      && dataSend.documento !== "" && dataSend.fechaNacimiento !== "" && dataSend.movil !== ""
      && dataSend.fijo !== "" && dataSend.correo !== "" && dataSend.singleSelect !== null
      && dataSend.sucursalSelect !== null && dataSend.regionSelect !== null) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          "nombres": dataSend.nombre,
          "apellidos": dataSend.apellidos,
          "idTipoDocumento": dataSend.tipoDocSelect.value,
          "numeroDocumento": dataSend.documento,
          "fechaNacimiento": dataSend.fechaNacimiento,
          "movil": dataSend.movil,
          "fijo": dataSend.fijo,
          "correo": dataSend.correo,
          "idEmpresa": dataSend.singleSelect.value,
          "idSucursal": dataSend.sucursalSelect.value,
          "hobbies": dataSend.hobbies,
          "region": dataSend.regionSelect.value,
          "idDistribuidor": dataSend.idperfil
        }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/dist/update", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("distForm").reset();
            this.setState({ nombre: "", apellidos: "", tipoDocSelect: null, documento: "", movil: "", fijo: "", correo: "", singleSelect: null, sucursalSelect: null, imgName: "", regionSelect: null, hobbies: "" });
            this.onLoadDist();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }

  }
  onInputNameChange(e) {
    this.setState({ nombre: e.target.value });
  }
  onInputLastNameChange(e) {
    this.setState({ apellidos: e.target.value });
  }
  onInputEmailChange(e) {
    this.setState({ correo: e.target.value });
  }
  onInputDateChange(e) {
    this.setState({ fechaNacimiento: e.target.value });
  }
  onInputDNChange(e) {
    this.setState({ documento: e.target.value });
  }
  onInputFijoChange(e) {
    this.setState({ fijo: e.target.value });
  }
  onInputMovilChange(e) {
    this.setState({ movil: e.target.value });
  }
  onInputHobbiesChange(e) {
    this.setState({ hobbies: e.target.value });
  }
  loadDataBranches(id) {
    let branches = [];
    this.state.branchestemp.map((prop, key) => {
      if (prop.id === id.id) {
        branches.push(prop)
      }
      return key;
    });
    this.setState({ singleSelect: id })
    this.setState({ branches });
  }
  onLoadDist() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/dist/", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const persons = res.data.response.map((prop, key) => {
            return {
              id: prop.idDistribuidor,
              nombre: prop.nombres,
              apellidos: prop.apellidos,
              fijo: prop.fijo,
              movil: prop.movil,
              hobbies: prop.hobbies,
              TipoDocumento: prop.TipoDocumento,
              documento: prop.numeroDocumento,
              fechaNacimiento: prop.fechaNacimiento,
              correo: prop.correo,
              empresa: prop.nombreEmpresa,
              sucursal: prop.nombreSucursal,
              region: prop.region,
              ciudad: prop.ciudad,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.persons.find(o => o.id === prop.idDistribuidor);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.persons.find(o => o.id === prop.idDistribuidor);
                      this.onRemoveDist(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ persons });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  onLoadEnterprise() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/enterprises/", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const enterprises = res.data.response.map((prop, key) => {
            return {
              value: prop.idEmpresa,
              id: prop.idEmpresa,
              label: prop.nombre,
              nombre: prop.nombre,
              NIT: prop.NIT,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.enterprises.find(o => o.id === prop.idEmpresa);
                      this.setEnterpriseValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.enterprises.find(o => o.id === prop.idEmpresa);
                      this.onRemoveEnterprises(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ enterprises });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  onLoadBranchs() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/branches/", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const branchestemp = res.data.response.map((prop, key) => {

            return {
              value: prop.idSucursal,
              id: prop.idEmpresa,
              label: prop.nombre,
              idSucursal: prop.idSucursal,
              ciudad: prop.ciudad,
              direccion: prop.direccion,
              nombre: prop.nombre,
              telefono: prop.telefono,
              correo:prop.correo,
              principal: (prop.isPrincipal === 1) ? true : false
            };
          });
          this.setState({ branchestemp });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  componentDidMount() {
    this.onLoadDist();
    this.onLoadEnterprise();
    this.onLoadBranchs();
  }
  render() {
    const { newProfile, newEnterprice, editNew, editEnterprise } = this.state;
    var yesterday = Datetime.moment().subtract(1, 'day');
    var valid = function (current) {
      return !current.isAfter(yesterday);
    };
    const tabs = (
      <Tab.Container id="tabs-with-dropdown" defaultActiveKey="depar">
        <Row className="clearfix">
          <Col sm={12}>
            <Nav bsStyle="tabs">
              <NavItem eventKey="depar">Departamentos</NavItem>
              <NavItem eventKey="ciu">Ciudades</NavItem>
            </Nav>
          </Col>
          <Col sm={12}>
            <Tab.Content animation>
              <Tab.Pane eventKey="depar">
                {!newProfile && (
                  <Row>
                    <Col md={12}>
                      <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                        Crear nuevo
                      </Button>
                    </Col>
                  </Row>
                )}
                {newProfile && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                      <Col md={12}>
                        {editNew && (
                          <h4 className="numbers text-danger">Editar distribuidor</h4>
                        )}
                        {!editNew && (
                          <h4 className="numbers text-danger">Crear distribuidor</h4>
                        )}
                      </Col>
                      <Col md={12}>
                        <Card shadow="ns"
                          content={
                            <form id="distForm">
                              <Col md={12}>
                                <Col md={4} className="pl5">
                                  <FormGroup>
                                    <ControlLabel>Nombre (s)</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={this.onInputNameChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Apellido (s)</ControlLabel>
                                    <FormControl placeholder="Escribe un apellido" value={this.state.apellidos} onChange={this.onInputLastNameChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Tipo de documento</ControlLabel>
                                    <Select placeholder="Seleccionar"
                                      name="tipoDocSelect"
                                      value={this.state.tipoDocSelect}
                                      options={this.state.tipos}
                                      onChange={(value) =>
                                        this.setState({ tipoDocSelect: value })
                                      }
                                    />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Número de documento</ControlLabel>
                                    <FormControl placeholder="Escribe un número de documento" value={this.state.documento} onChange={this.onInputDNChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                {editNew && (
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Fecha de nacimiento</ControlLabel>
                                      <Datetime
                                        locale="es"
                                        onChange={(e) => this.setState({ fechaNacimiento: e.format("DD/MM/YYYY") })}
                                        value={this.state.fechaNacimiento}
                                        timeFormat={false}
                                        isValidDate={valid}
                                        inputProps={{ placeholder: "Escoge una fecha", id: "fechaNacimiento" }}
                                      />
                                    </FormGroup>
                                  </Col>
                                )}
                                {!editNew && (
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Fecha de nacimiento</ControlLabel>
                                      <Datetime
                                        locale="es"
                                        timeFormat={false}
                                        isValidDate={valid}
                                        inputProps={{ placeholder: "Escoge una fecha", id: "fechaNacimiento" }}
                                      />
                                    </FormGroup>
                                  </Col>
                                )}
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Teléfono móvil</ControlLabel>
                                    <FormControl placeholder="Escribe un número móvil" value={this.state.movil} onChange={this.onInputMovilChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Teléfono fijo</ControlLabel>
                                    <FormControl placeholder="Escribe un número fijo" value={this.state.fijo} onChange={this.onInputFijoChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>

                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>Correo electrónico</ControlLabel>
                                    <FormControl placeholder="Escribe un correo" value={this.state.correo} onChange={this.onInputEmailChange.bind(this)} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <ControlLabel>Empresa</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="singleSelect"
                                    value={this.state.singleSelect}
                                    options={this.state.enterprises}
                                    onChange={(idEmpresa) =>
                                      this.loadDataBranches(idEmpresa)
                                    }
                                  />
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={4}>
                                  <ControlLabel>Sucursal</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="sucursalSelect"
                                    value={this.state.sucursalSelect}
                                    options={this.state.branches}
                                    onChange={value =>
                                      this.setState({ sucursalSelect: value })
                                    }
                                  />
                                </Col>
                                {!editNew && (
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Foto</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFiles.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                )}
                                <Col md={4}>
                                  <ControlLabel>Región</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="sucursalSelect"
                                    value={this.state.regionSelect}
                                    options={this.state.regiones}
                                    onChange={value =>
                                      this.setState({ regionSelect: value })
                                    }
                                  />
                                </Col>

                              </Col>
                              <Col md={12} className="mb-2">
                                <Col md={12} >
                                  <FormGroup>
                                    <ControlLabel>Hobbies</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      value={this.state.hobbies} onChange={this.onInputHobbiesChange.bind(this)}
                                      componentClass="textarea"
                                      bsClass="form-control rsn"
                                      placeholder="Escriba hobbies"
                                    />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                            </Button>
                                </Col>
                                {!editNew && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" type="button" onClick={this.onSubmitForm.bind(this)} bsSize="large" fill >
                                      Agregar
                                    </Button>
                                  </Col>
                                )}
                                {editNew && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" type="button" onClick={this.onSubmitUpdateForm} bsSize="large" fill >
                                      Guardar
                                    </Button>
                                  </Col>
                                )}
                              </Col>
                            </form>
                          }
                        />

                      </Col>

                    </Row>
                  </BlockUi>)}
                {!newProfile && (
                  <Row>
                    <Col md={12}>
                      <Card
                        content={
                          <ReactTable
                            data={this.state.persons}
                            filterable
                            columns={[
                              {
                                Header: "Departamento",
                                accessor: "empresa",
                                minWidth: 20,
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay información disponible'
                            pageText='Página'
                            ofText='de'
                            rowsText='filas'
                            defaultFilterMethod={this.filterMethod.bind(this)}
                            // Accessibility Labels
                            pageJumpText='ir a la página'
                            rowsSelectorText='filas por página'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        }
                      />
                    </Col>
                  </Row>
                )}
              </Tab.Pane>
              <Tab.Pane eventKey="ciu">
                {!newEnterprice && (
                  <Row>
                    <Col md={12}>
                      <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleEnterprice}>
                        Crear nueva
                      </Button>
                    </Col>
                  </Row>)}
                {newEnterprice && (
                  <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                      <Col md={12}>
                        {editEnterprise && (
                          <h4 className="numbers text-danger">Editar empresa</h4>
                        )}
                        {!editEnterprise && (
                          <h4 className="numbers text-danger">Crear empresa</h4>
                        )}
                      </Col>
                      <Col md={12}>
                        <Card shadow="ns"
                          content={
                            <form id="branchesForm">
                              <Col md={12}>
                                <Col md={4} className="pl5">
                                  <FormGroup>
                                    <ControlLabel>Nombre /razón social</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre o razón social" value={this.state.razon} onChange={(e) => { this.setState({ razon: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={4}>
                                  <FormGroup>
                                    <ControlLabel>NIT</ControlLabel>
                                    <FormControl placeholder="Escribe un NIT" value={this.state.nit} onChange={(e) => { this.setState({ nit: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                              <Col md={12}>
                                <h4 className="mtext numbers text-danger">Sucursal</h4>
                              </Col>
                              <Col md={12} >
                                <Col md={12} className="pl5">
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Nombre</ControlLabel>
                                      <FormControl id="nombre" placeholder="Escribe un nombre" value={this.state.nombreSucursal} onChange={(e) => { this.setState({ nombreSucursal: e.target.value }) }} type="text" />
                                    </FormGroup>
                                  </Col>

                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Ciudad</ControlLabel>
                                      <FormControl id="ciudad" placeholder="Escribe una ciudad" value={this.state.ciudadSucursal} onChange={(e) => { this.setState({ ciudadSucursal: e.target.value }) }} type="text" />
                                    </FormGroup>
                                  </Col>
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Dirección</ControlLabel>
                                      <FormControl id="direccion" placeholder="Escribe una dirección" value={this.state.direccionSucursal} onChange={(e) => { this.setState({ direccionSucursal: e.target.value }) }} type="text" />
                                    </FormGroup>
                                  </Col>
                                </Col>
                                <Col md={12}>
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Teléfono</ControlLabel>
                                      <FormControl id="telefono" placeholder="Escribe un numero de telefono" value={this.state.telefonoSucursal} onChange={(e) => { this.setState({ telefonoSucursal: e.target.value }) }} type="number" />
                                    </FormGroup>
                                  </Col>
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Correo electrónico</ControlLabel>
                                      <FormControl id="correo" placeholder="Escribe un correo" value={this.state.correoSucursal} onChange={(e) => { this.setState({ correoSucursal: e.target.value }) }} type="text" />
                                    </FormGroup>
                                  </Col>
                                  {!editEnterprise && (
                                  <Col md={4}>
                                    <FormGroup>
                                      <ControlLabel>Sucursal principal</ControlLabel>
                                      <Checkbox
                                        number="principal"
                                      />
                                    </FormGroup>
                                  </Col>
                                  )}
                                </Col>
                              </Col>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleEnterprice}>
                                    Cancelar
                                  </Button>
                                </Col>

                                <Col md={5}>
                                  {editEnterprise && (
                                    <Button bsStyle="danger" type="button" onClick={this.onSubmitUpdateBranchesForm.bind(this)} bsSize="large" fill >
                                      Guardar
                                    </Button>
                                  )}
                                  {!editEnterprise && (
                                    <Button bsStyle="danger" type="button" onClick={this.onSubmitBranchesForm.bind(this)} bsSize="large" fill >
                                      Agregar
                                    </Button>
                                  )}

                                </Col>
                              </Col>
                            </form>
                          }
                        />

                      </Col>

                    </Row>
                  </BlockUi>)}
                {!newEnterprice && (
                  <Row>
                    <Col md={12}>
                      <Card
                        content={
                          <ReactTable
                            data={this.state.enterprises}
                            filterable
                            columns={[
                              {
                                Header: "Departamento",
                                accessor: "nombre",
                                minWidth: 20,
                              },
                              {
                                Header: "Ciudad",
                                accessor: "NIT",
                                minWidth: 20,
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay información disponible'
                            pageText='Página'
                            ofText='de'
                            rowsText='filas'
                            defaultFilterMethod={this.filterMethod.bind(this)}
                            // Accessibility Labels
                            pageJumpText='ir a la página'
                            rowsSelectorText='filas por página'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        }
                      />
                    </Col>
                  </Row>)}
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    );
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card content={tabs} />
            </Col>
          </Row>

        </Grid>
      </div>
    );
  }
}

export default ReactTables;
