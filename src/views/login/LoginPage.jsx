import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";
import axios from 'axios';
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import logo from "assets/img/logoIni.png";
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import SweetAlert from "react-bootstrap-sweetalert";

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardHidden: true,
      blocking: false,
      alert: null,
      correo: "",
      pass: ""
    };
    this.hideAlert=this.hideAlert.bind(this);
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }
  redireccionar() {
    const { correo, pass } = this.state;
    if (correo !== "" && pass !== "") {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          correo,
          pass
        }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/login", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            sessionStorage.Cookie = JSON.stringify(res.data.response);
            window.location.href = "/ci_ciadmin/dashboard";
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No pudimos procesar tu solicitud por favor intentalo nuevamente.
              </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  render() {
    return (
      <Grid>
        {this.state.alert}
        <Row>
          <Col md={4} mdOffset={4}>
            <img src={logo} className="w100p" alt="logo ideas" />
          </Col>


          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <BlockUi tag="div" blocking={this.state.blocking}>
              <form>

                <Card
                  hidden={this.state.cardHidden}
                  className="pt0"
                  shadow="ns"
                  hCenter
                  category="Ingresa tus datos de acceso"
                  textCenter
                  content={
                    <div>
                      <FormGroup>
                        <ControlLabel className="cgray ntt fs15">Correo electrónico</ControlLabel>
                        <FormControl className="login-input bsinput" value={this.state.correo} onChange={(e) => this.setState({ correo: e.target.value })} placeholder="Escribe tu correo electrónico" type="email" />
                      </FormGroup>
                      <FormGroup>
                        <ControlLabel className="cgray ntt fs15">Contraseña</ControlLabel>
                        <FormControl className="login-input bsinput" placeholder="Ingresa tu contraseña" value={this.state.pass} onChange={(e) => this.setState({ pass: e.target.value })} type="password" />
                      </FormGroup>
                    </div>
                  }
                  legend={
                    <div className="col-md-12 p0">
                      <Button bsStyle="danger" className="btn-fill text-center" bsSize="large" fill wd="true" onClick={this.redireccionar.bind(this)}>
                        Iniciar sesión
                  </Button>
                    </div>
                  }
                  ftTextCenter
                />

              </form>
            </BlockUi>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default LoginPage;
