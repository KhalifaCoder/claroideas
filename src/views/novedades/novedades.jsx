import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], circularesInactivas: [], newCircular: true, titulo: "", texto: "", alert: null, idSelected: "", viewOthers: false, editCircular: false };
    this.toggleBox = this.toggleBox.bind(this);
    this.toggleOthers = this.toggleOthers.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.deleteCirculars = this.deleteCirculars.bind(this);
    this.inactiveCirculars = this.inactiveCirculars.bind(this);

  }
  toggleBox() {
    const { newCircular } = this.state;
    if (newCircular === false) {
      this.setState({ titulo: "", texto: "" });
      this.setState({ editCircular: false });
    }
    this.setState({ newCircular: !newCircular });
  }
  toggleOthers() {
    this.setState({ viewOthers: !this.state.viewOthers });
  }

  hideAlert() {
    this.setState({
      alert: null
    });
  }
  onSubmitForm(e) {
    var dataSend = this.state;
    if (dataSend.texto !== "" && dataSend.titulo !== "") {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          "titulo": dataSend.titulo,
          "texto": dataSend.texto
        }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/circulars/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onLoadCirculars();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }

  }
  setValues(data) {
    if (data.texto) {
      const { titulo, texto, id } = data;
      this.setState({ titulo });
      this.setState({ texto });
      this.setState({ idSelected: id });
      this.setState({ editCircular: !this.state.editCircular });
      this.toggleBox();
    } else {
      this.setState({ titulo: "", texto: "", idSelected: "" });
    }
  }
  filterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;
    if(row[id] !== undefined){
      if(String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }
  onUpdateCirculars() {
    const { titulo, texto, idSelected } = this.state;
    if (texto !== "" && titulo !== "" && idSelected !== "") {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { titulo, texto, "idcircular": idSelected }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/circulars/update", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onLoadCirculars();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onRemoveCirculars(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteCirculars(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }
  onInactivateCirculars(id, estado) {
    var text = "Esta accion inactivara esta novedad.";
    if (estado === 1) {
      text = "Esta accion activara esta novedad.";
    }
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.inactiveCirculars(id, estado)}
          onCancel={() => this.hideAlert()}
        >
          {text}
    </SweetAlert>
      )
    });
  }

  inactiveCirculars(id, estado) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idcircular": id, "state": estado }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/circulars/inactive", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
              this.onLoadCirculars();
              this.onLoadInactiveCirculars();
            
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }

  deleteCirculars(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idcircular": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/circulars/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onLoadCirculars();
              this.onLoadInactiveCirculars();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onLoadInactiveCirculars() {
    axios.get("https://www.ideasqueactivanclaro.com.co/app/general/circulars/inactiveCirculars", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const circularesInactivas = res.data.response.map((prop, key) => {
            return {
              id: prop.idcircular,
              titulo: prop.titulo,
              texto: prop.texto,
              datepublic: prop.fechaPublicacion,
              actions: (
                <div className="actions-right">
                  <Button
                    onClick={() => {
                      let obj = this.state.circularesInactivas.find(o => o.id === prop.idcircular);
                      this.onInactivateCirculars(obj.id, 1);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-check" />
                  </Button>
                  <Button
                    onClick={() => {
                      let obj = this.state.circularesInactivas.find(o => o.id === prop.idcircular);
                      this.onRemoveCirculars(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                </div>
              )
            };
          });
          this.setState({ circularesInactivas });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  onLoadCirculars() {
    axios.get("https://www.ideasqueactivanclaro.com.co/app/general/circulars/", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const circulares = res.data.response.map((prop, key) => {
            return {
              id: prop.idcircular,
              titulo: prop.titulo,
              texto: prop.texto,
              datepublic: prop.fechaPublicacion,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.circulares.find(o => o.id === prop.idcircular);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.circulares.find(o => o.id === prop.idcircular);
                      this.onInactivateCirculars(obj.id, 0);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-archive" />
                  </Button>
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.circulares.find(o => o.id === prop.idcircular);
                      this.onRemoveCirculars(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ circulares });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
          )
        });
      });
  }
  componentDidMount() {
    this.onLoadCirculars();
    this.onLoadInactiveCirculars();
  }
  render() {
    const { newCircular, editCircular, viewOthers } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              {viewOthers  && (
                 <Col md={12}>
                 <Card
                   content={
                     <Row>
                  <Col md={12}>
                    <h4 className="numbers text-danger media">Novedades inactivas</h4>
                    <Button className="pull-right mb-2 btn-fill" bsStyle="info" fill wd="true" onClick={this.toggleOthers}>
                      Volver
                          </Button>
                  </Col>
                  <Col md={12} xs={12}>
                    <ReactTable
                      data={this.state.circularesInactivas}
                      filterable
                      columns={[
                        {
                          Header: "Título/nombre",
                          accessor: "titulo"
                        },
                        {
                          Header: "Creada",
                          accessor: "datepublic",
                        },
                        {
                          Header: "",
                          accessor: "actions",
                          sortable: false,
                          filterable: false
                        }
                      ]}
                      defaultPageSize={10}
                      showPaginationTop={false}
                      previousText="Anterior"
                      nextText="Siguiente"
                      loadingText='Cargando...'
                      noDataText='No hay información disponible'
                      pageText='Página'
                      ofText='de'
                      rowsText='filas'
                      defaultFilterMethod={this.filterMethod.bind(this)}
                      // Accessibility Labels
                      pageJumpText='ir a la página'
                      rowsSelectorText='filas por página'
                      showPaginationBottom
                      className="-striped -highlight"
                    />
                  </Col>
                </Row>
                   }
                />
                </Col>
              )}
              {!newCircular && !viewOthers && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <BlockUi tag="div" blocking={this.state.blocking}>
                          <form>
                            {!editCircular && (
                              <Row>
                                <Col md={12} className="mb-2">
                                  <h4 className="numbers text-danger media">Crear novedad</h4>
                                </Col>
                              </Row>)}
                            {editCircular && (
                              <Row>
                                <Col md={12} className="mb-2">
                                  <h4 className="numbers text-danger media">Editar novedad</h4>
                                </Col>
                              </Row>)}

                            <Row>
                              <Col md={12} className="pl25">
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Titular novedad</ControlLabel>
                                    <FormControl placeholder="Titular novedad" value={this.state.titulo} onChange={(e) => { this.setState({ titulo: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12} className="pl15">
                                <Col md={12}>
                                  <FormGroup>
                                    <ControlLabel>Texto principal</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      placeholder="Texto principal"
                                      type="text"
                                      componentClass="textarea"
                                      value={this.state.texto} onChange={(e) => { this.setState({ texto: e.target.value }) }} />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                          </Button>
                                </Col>
                                {!editCircular && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm.bind(this)}>
                                      Agregar
                            </Button>
                                  </Col>)}
                                {editCircular && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onUpdateCirculars.bind(this)}>
                                      Guardar
                                  </Button>
                                  </Col>)}
                              </Col>
                            </Row>

                          </form>
                        </BlockUi>
                      }
                    />
                  </Col>
                </Row>)}
              {newCircular && !viewOthers && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nuevo
                          </Button>
                            <Button className="pull-right mb-2 btn-fill mr20" bsStyle="warning" fill wd="true" onClick={this.toggleOthers}>
                              Ver Archivadas
                          </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.circulares}
                              filterable
                              columns={[
                                {
                                  Header: "Título/nombre",
                                  accessor: "titulo"
                                },
                                {
                                  Header: "Creada",
                                  accessor: "datepublic",
                                },
                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay información disponible'
                              pageText='Página'
                              ofText='de'
                              rowsText='filas'
                              defaultFilterMethod={this.filterMethod.bind(this)}
                              // Accessibility Labels
                              pageJumpText='ir a la página'
                              rowsSelectorText='filas por página'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
