import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";

import Button from "components/CustomButton/CustomButton.jsx";

class Notifications extends Component {
  cerrarSesion() {
    sessionStorage.clear();
    window.location.href = "/ci_ciadmin";
  }
  render() {
    return (
      <div className="content">
        <Grid fluid>
          <div className="card">
            <div className="content">
              <div className="places-buttons">
                <Row className="mt3p mb-2">
                  <Col md={8} mdOffset={2} className="text-center">
                    <h3 className="fwbold">
                    ¿Estás seguro que deseas terminar la sesión actual?</h3>
                  </Col>
                </Row>
                <Row className="mt3p mb-2">
                  <Col md={6} mdOffset={3} className="text-center">
                    <Button bsStyle="danger" bsSize="large" fill onClick={this.cerrarSesion.bind(this)}>
                      Aceptar
                    </Button>
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        </Grid>
      </div>
    );
  }
}

export default Notifications;
