import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup,Thumbnail, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import Select from "react-select";
import SweetAlert from "react-bootstrap-sweetalert";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      proveedores: [],
      newProveedor: true,
      nombre: "",
      telefono: "",
      correo: "",
      alert: null,
      imgName: "",
      file: {},
      editProv: false,
      categories: [],
      responsable: ""
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.onSubmitUpdateForm = this.onSubmitUpdateForm.bind(this);
    this.editar = this.editar.bind(this);
    this.updateImage = this.updateImage.bind(this);
  }
  toggleBox() {
    const { newProveedor, editProv } = this.state;
    if (editProv === true) {
      this.setState({ editProv: false });
      this.setState({ nombre: "", telefono: "", correo: "", responsable: "", file: {}, imgName: "", singleSelect: null });
    }
    this.setState({ newProveedor: !newProveedor });
  }

  hideAlert() {
    this.setState({
      alert: null
    });
  }
  setValues(data) {
    if (data) {
      const { name, number, email, img, responsable, id,categoria } = data;
      var categoriaAct;
      for(let a in this.state.categories){
        if(this.state.categories[a].value===categoria){
          categoriaAct=this.state.categories[a];
        }
      }
      this.setState({ nombre: name, telefono: number, responsable, correo: email, imgPreName: img, idproveedor: id,singleSelect:categoriaAct});
      this.setState({ editProv: !this.state.editProv });
      this.toggleBox();
    }
  }
  getImg(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file: { extencion, base64 } })
  }
  editar() {
    this.setState({ editarImg: !this.state.editarImg })
  }
  updateImage() {
    if (this.state.file && this.state.file.base64) {
      this.updateImg(this.state.file);
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  updateImg(file){
    this.setState({ blocking: !this.state.blocking });
    let fields = {
      data: { file,idproveedor :this.state.idproveedor}
    };
    axios.post("https://www.ideasqueactivanclaro.com.co/admin/suppliers/updateImg", fields)
      .then(res => {
        var resp = res.data.response;
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error === 0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
          setTimeout(this.onLoadSuppliers(), 500);
          this.setState({imgPreName:file.base64});
          this.editar();          
        } else {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                danger
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"

              >
                {resp}
              </SweetAlert>
            )
          });
        }
      }).catch(error => {
        this.setState({ blocking: false});
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
}
  onLoadCategories() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/suppliers/categories", {headers: {"x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI="}})
      .then(res => {
        if (res.data.response) {
          const categories = res.data.response.map((prop) => {
            return {
              value: prop.idcategoria,
              label: prop.nombre
            };
          });
          this.setState({ categories });
        }
      }).catch(error => {
        this.setState({ blocking: false});
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
  }

  onRemoveProvedor(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteProv(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }

  deleteProv(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idproveedor": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/suppliers/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onLoadSuppliers(), 500);
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  filterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;
    if(row[id] !== undefined){
      if(String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }
  getFile1(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file: { extencion, base64 } })
  }
  onSubmitUpdateForm() {
    const { nombre, responsable, telefono, correo, singleSelect, idproveedor } = this.state;
    if (nombre !== "" && telefono !== "" && correo !== ""
      && responsable !== "" && singleSelect !== null) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          nombre, telefono, correo, responsable, "idcategoria": singleSelect.value, idproveedor
        }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/suppliers/update", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("provForm").reset();
            this.setState({ nombre: "", telefono: "", correo: "", responsable: "", file: {}, imgName: "", singleSelect: null });
            this.onLoadSuppliers();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  onSubmitForm(e) {
    const { nombre, responsable, telefono, correo, singleSelect, file } = this.state;
    if (nombre !== "" && telefono !== "" && correo !== ""
      && responsable !== "" && singleSelect !== null && file !== {}) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: {
          nombre, telefono, correo, responsable, "idcategoria": singleSelect.value, img: file
        }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/suppliers/", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("provForm").reset();
            this.setState({ nombre: "", telefono: "", correo: "", responsable: "", file: {}, imgName: "", singleSelect: null });
            this.onLoadSuppliers();
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false});
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"
                >
                  No se recibió información. Inténtalo de nuevo.
                </SweetAlert>
              )
            });
          });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Por favor ingresa todos los campos"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }

  }

  onLoadSuppliers() {
    axios.get("https://www.ideasqueactivanclaro.com.co/app/general/suppliers/", {headers: {"x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI="}})
      .then(res => {
        if (res.data.response.proveedores) {
          const proveedores = res.data.response.proveedores.map((prop, key) => {
            return {
              id: prop.idproveedor,
              name: prop.nombre,
              number: prop.telefono,
              email: prop.correo,
              img: prop.img,
              categoria:prop.idCategoria,
              responsable: prop.responsable,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.proveedores.find(o => o.id === prop.idproveedor);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.proveedores.find(o => o.id === prop.idproveedor);
                      this.onRemoveProvedor(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ proveedores });
        }
      }).catch(error => {
        this.setState({ blocking: false});
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
  }
  componentDidMount() {
    this.onLoadCategories();
    this.onLoadSuppliers();
  }
  render() {
    const { newProveedor, editProv,editarImg } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              {!newProveedor && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <form id="provForm">
                          <Col md={10} className="mb-2">
                            {!editProv && (
                              <h4 className="numbers ml0 text-danger media">Crear proveedor</h4>
                            )}
                            {editProv && (
                              <h4 className="numbers ml0 text-danger media">Editar proveedor</h4>
                            )}
                          </Col>

                          <BlockUi tag="div" blocking={this.state.blocking}>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Nombre de proveedor</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.nombre} onChange={(e) => { this.setState({ nombre: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Número de telefono</ControlLabel>
                                    <FormControl placeholder="Escribe un número de teléfono" value={this.state.telefono} onChange={(e) => { this.setState({ telefono: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <ControlLabel>Categoría</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="singleSelect"
                                    value={this.state.singleSelect}
                                    options={this.state.categories}
                                    onChange={(value) =>
                                      this.setState({ singleSelect: value })
                                    }
                                  />
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Correo electrónico</ControlLabel>
                                    <FormControl placeholder="Escribe un correo" value={this.state.correo} onChange={(e) => { this.setState({ correo: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>

                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Responsable de la región</ControlLabel>
                                    <FormControl placeholder="Escribe un nombre" value={this.state.responsable} onChange={(e) => { this.setState({ responsable: e.target.value }) }} type="text" />
                                  </FormGroup>
                                </Col>
                                {!editProv && (
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Imagen principal</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFile1.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                )}
                              </Col>
                            </Row>
                            {editProv && (
                              <Row>
                                <Col md={8} className="mb-2">
                                <Col md={6}>
                                  {!editarImg && (
                                    <Thumbnail src={this.state.imgPreName} alt="242x200">
                                      <h3 className="text-center">Imagen</h3>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editar}
                                          bsStyle="primary"
                                          simple><i className="fa fa-edit"></i></Button>
                                      </p>
                                    </Thumbnail>
                                  )}
                                  {editarImg && (
                                    <div className="thumbnail">
                                      <h3 className="text-center">Imagen</h3>
                                      <div className="custom_file_upload mb-2">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getImg.bind(this)} />
                                        </div>

                                      </div>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editar}
                                          bsStyle="danger"
                                          simple><i className="fa fa-times fa-2x"></i></Button>
                                        <Button
                                          onClick={this.updateImage}
                                          bsStyle="success"
                                          simple><i className="fa fa-check fa-2x"></i></Button>
                                      </p>
                                    </div>
                                  )}
                                </Col>
                              </Col>
                              </Row>
                            )}
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                          </Button>
                                </Col>
                                {!editProv && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm.bind(this)}>
                                      Publicar
                            </Button>
                                  </Col>)}
                                {editProv && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                      Guardar
                            </Button>
                                  </Col>
                                )}
                              </Col>
                            </Row>

                          </BlockUi>
                        </form>
                      }
                    />
                  </Col>
                </Row>)}
              {newProveedor && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <Row>
                          <Col md={12}>
                            <Button className="pull-right mb-2 btn-fill" bsStyle="danger" fill wd="true" onClick={this.toggleBox}>
                              Crear nuevo
                          </Button>
                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.proveedores}
                              filterable
                              columns={[
                                {
                                  Header: "Nombre proveedor",
                                  accessor: "name"
                                },
                                {
                                  Header: "Número contacto",
                                  accessor: "number"
                                },
                                {
                                  Header: "Correo electrónico",
                                  accessor: "email",
                                },
                                {
                                  Header: "Responsable de la región",
                                  accessor: "responsable",
                                },
                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay información disponible'
                              pageText='Página'
                              ofText='de'
                              rowsText='filas'
                              defaultFilterMethod={this.filterMethod.bind(this)}
                              // Accessibility Labels
                              pageJumpText='ir a la página'
                              rowsSelectorText='filas por página'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
