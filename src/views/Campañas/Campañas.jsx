import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import { Grid, Row, Col, FormGroup, Thumbnail, NavDropdown, MenuItem, ControlLabel, FormControl } from "react-bootstrap";
import axios from 'axios';
import Select from "react-select";
import Datetime from "react-datetime";
import "react-select/dist/react-select.css";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import SweetAlert from "react-bootstrap-sweetalert";
import FileBase64 from 'react-file-base64';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

import Arrow from "assets/img/left-arrow.png";
require('moment');
require('moment/locale/es');
class ReactTables extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      preview: false,
      editCam: false,
      imgPreName: "",
      filePre: {},
      imgPriName: "",
      filePri: {},
      icon: {},
      nombreCategoria: "",
      iconName: "",
      newCategoria: false,
      editarImgPre: false,
      editarImgPri: false,
      imgName: "",
      link: "",
      categories: [],
      categoriaSelect: null,
      alert: null,
      imgName2: "",
      file1: {}, file2: {}, blocking: false, nombre: "", textoprevio: "", textoprincipal: "", img1: "", img2: "",
      campañas: [],
      viewOthers: false,
      fechaIni: "",
      fechaFin: "",
      idcampaña: "",
      contador:0,
      newCampaña: false,
      regionSelect: null,
      singleSelect: null,
      posicionSelect: null,
      posiciones: [{ "label": "1", "value": "1" },
      { "label": "2", "value": "2" }],
      tipos: [{ "label": "1", "value": "1" },
      { "label": "2", "value": "2" },
      { "label": "3", "value": "3" }]
    };
    this.toggleBox = this.toggleBox.bind(this);
    this.togglePreview = this.togglePreview.bind(this);
    this.toggleOthers = this.toggleOthers.bind(this);
    this.toggleCategory = this.toggleCategory.bind(this);
    this.editarPre = this.editarPre.bind(this);
    this.updateImgPre = this.updateImgPre.bind(this);
    this.updateImgPri = this.updateImgPri.bind(this);
    this.editarPri = this.editarPri.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.toggleOthers = this.toggleOthers.bind(this);
    this.onSubmitUpdateForm = this.onSubmitUpdateForm.bind(this);
    this.onSubmitNewCategory = this.onSubmitNewCategory.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }

  filterMethod = (filter, row, column) => {
    const id = filter.pivotId || filter.id;
    if (row[id] !== undefined) {
      if (String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].toLowerCase()).includes(filter.value.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
  toggleOthers() {
    this.setState({ viewOthers: !this.state.viewOthers });
  }

  getIcon(files) {
    this.setState({ iconName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ icon: { extencion, base64 } })
  }
  onSubmitNewCategory() {
    const { nombreCategoria, icon } = this.state;
    if (nombreCategoria !== "" && icon.base64) {
      let dataSend = { data: { nombreCategoria, icon } };
      this.setState({ blocking: !this.state.blocking });
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/campaigns/categories", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.setState({ iconName: "", icon: {} });
            this.onLoadCategories();
            this.toggleCategory();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                Categoría de campaña creada correctamente.
              </SweetAlert>
            )
          });
          this.setState({ iconName: "", icon: {} });
          this.onLoadCategories();
          this.toggleCategory();
          /* this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          }); */
        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  toggleBox() {
    const { newCampaña, editCam } = this.state;
    if (editCam === true) {
      this.setState({ editCam: false });
      this.setState({ nombre: "", textoprevio: "", textoprincipal: "", img1: "", img2: "", link: "", categoriaSelect: null, singleSelect: null, posicionSelect: null });
      document.getElementById('fechaIni').value = "";
      document.getElementById('fechaFin').value = "";
    }
    this.setState({ newCampaña: !newCampaña });
  }
  toggleCategory() {
    this.setState({ newCategoria: !this.state.newCategoria });
  }
  editarPre() {
    this.setState({ editarImgPre: !this.state.editarImgPre })
  }
  editarPri() {
    this.setState({ editarImgPri: !this.state.editarImgPri })
  }
  togglePreview() {
    this.setState({ preview: !this.state.preview })
  }
  hideAlert() {
    this.setState({
      alert: null
    });
  }
  onLoadCategories() {
    axios.get("https://www.ideasqueactivanclaro.com.co/admin/campaigns/categories", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const categories = res.data.response.map((prop) => {
            return {
              value: prop.idcategoria,
              label: prop.nombre
            };
          });
          this.setState({ categories });
        }
      }).catch(error => {

        this.setState({ blocking: !this.state.blocking });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
            </SweetAlert>
          )
        });
      });
  }

  onSubmitUpdateForm() {
    var fechaIni = document.getElementById('fechaIni').value;
    fechaIni = this.formatDate(fechaIni);
    fechaIni = Datetime.moment(fechaIni).format();
    var fechaFin = document.getElementById('fechaFin').value;
    fechaFin = this.formatDate(fechaFin);
    fechaFin = Datetime.moment(fechaFin).format();
    const { nombre, textoprevio, textoprincipal, singleSelect, idcampaña, categoriaSelect, link, posicionSelect } = this.state;
    if (nombre !== "" && textoprevio !== "" && textoprincipal !== "" && singleSelect != null && link !== "" && categoriaSelect !== null && fechaIni !== "" && fechaFin !== "") {
      this.setState({ blocking: !this.state.blocking });
      var posicion;
      if (posicionSelect) {
        posicion = posicionSelect.value;
      } else {
        posicion = 0;
      }
      let dataSend = { data: { nombre, textoprevio, textoprincipal, tipo: singleSelect.value, idcampaña, link, categoria: categoriaSelect.value, fechaIni, fechaFin, posicion } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/campaigns/update", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadCam();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("campaingsForm").reset();
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", textoprevio: "", textoprincipal: "", singleSelect: null, categoriaSelect: null, link: "" });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }
  updateImg(file, tipo) {
    this.setState({ blocking: !this.state.blocking });
    let fields = {
      data: { file, tipo, idcampaña: this.state.idcampaña }
    };
    axios.post("https://www.ideasqueactivanclaro.com.co/admin/campaigns/updateImg", fields)
      .then(res => {
        var resp = res.data.response;
        this.setState({ blocking: !this.state.blocking });
        if (res.data.error === 0) {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                success
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="success"
                confirmBtnText="Aceptar"
              >
                {resp}
              </SweetAlert>
            )
          });
          setTimeout(this.onloadCam(), 500);
          if (tipo === "previo") {
            this.setState({ img1: file.base64 });
            this.editarPre();
          } else {
            this.setState({ img2: file.base64 });
            this.editarPri();
          }

        } else {
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                danger
                title=""
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"

              >
                {resp}
              </SweetAlert>
            )
          });
        }
      }).catch(error => {
        this.setState({ blocking: !this.state.blocking });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
            </SweetAlert>
          )
        });
      });
  }
  updateImgPre() {
    if (this.state.filePre && this.state.filePre.base64) {
      this.updateImg(this.state.filePre, 'previo');
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  onInactivateCam(id, estado) {
    var text = "Esta acción archivará esta Campaña.";
    if (estado === 1) {
      text = "Esta acción activara esta Campaña.";
    }
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.inactiveCam(id, estado)}
          onCancel={() => this.hideAlert()}
        >
          {text}
        </SweetAlert>
      )
    });
  }

  inactiveCam(id, estado) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idcampaña": id, "state": estado }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/campaigns/inactive", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            this.onloadCam();
            this.onloadIncativeCam();

          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }

  updateImgPri() {
    if (this.state.filePri.base64) {
      this.updateImg(this.state.filePri, 'principal');
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Debes seleccionar una imagen.
          </SweetAlert>
        )
      });
    }
  }
  formatDate(date) {
    date = date.split("/");
    date = `${date[2]}-${date[1]}-${date[0]}`
    return date;
  }
  onSubmitForm() {
    var fechaIni = document.getElementById('fechaIni').value;
    fechaIni = this.formatDate(fechaIni);
    fechaIni = Datetime.moment(fechaIni).format();
    var fechaFin = document.getElementById('fechaFin').value;
    fechaFin = this.formatDate(fechaFin);
    fechaFin = Datetime.moment(fechaFin).format();
    const { file1, file2, nombre, textoprevio, textoprincipal, singleSelect, link, categoriaSelect, posicionSelect } = this.state;
    if (file1 !== "" && file2 !== "" && nombre !== "" && textoprevio !== "" && textoprincipal !== "" && singleSelect !== null && link !== "" && categoriaSelect !== null && fechaIni !== "" && fechaFin !== "") {
      this.setState({ blocking: !this.state.blocking });
      var posicion;
      if (posicionSelect) {
        posicion = posicionSelect.value;
      } else {
        posicion = 0;
      }
      let dataSend = { data: { file1, file2, nombre, textoprevio, textoprincipal, tipo: singleSelect.value, link, cctegoria: categoriaSelect.value, fechaIni, fechaFin, posicion } };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/campaigns/", dataSend)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.onloadCam();
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            document.getElementById("campaingsForm").reset();
            this.setState({ imgName: "", imgName2: "", file1: "", file2: "", nombre: "", idnoticia: "", textoprevio: "", textoprincipal: "", singleSelect: null, link: "", categoriaSelect: null });
            this.toggleBox();
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });
    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title=""
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"
          >
            Por favor ingresa todos los campos
          </SweetAlert>
        )
      });
    }
  }

  getFile1(files) {
    this.setState({ imgName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file1: { extencion, base64 } })
  }
  getImgPre(files) {
    this.setState({ imgPreName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePre: { extencion, base64 } })
  }

  getImgPri(files) {
    this.setState({ imgPriName: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ filePri: { extencion, base64 } })
  }

  getFile2(files) {
    this.setState({ imgName2: files.name });
    let extencion = files.name.split('.').pop();
    let base64 = files.base64;
    this.setState({ file2: { extencion, base64 } })
  }
  setValues(data) {
    if (data) {
      const { title, textoprevio, textoprincipal, img1, img2, id, link, dateStart, dateEnd, tipo, idCategory, posicion } = data;
      var tipoAct, categoriaAct, posicionAct;
      for (let i in this.state.tipos) {
        if (this.state.tipos[i].value == tipo) {
          tipoAct = this.state.tipos[i];
        }
      }
      for (let d in this.state.categories) {
        if (this.state.categories[d].label === idCategory) {
          categoriaAct = this.state.categories[d];
        }
      }
      for (let a in this.state.posiciones) {
        if (this.state.posiciones[a].value == posicion) {
          posicionAct = this.state.posiciones[a];
        }
      }
      this.setState({ nombre: title, textoprevio, textoprincipal, img1, img2, idcampaña: id, link, fechaIni: dateStart, fechaFin: dateEnd, categoriaSelect: categoriaAct, singleSelect: tipoAct, posicionSelect: posicionAct });
      this.setState({ editCam: !this.state.editCam });
      this.toggleBox();
    }
  }

  onRemoveCampaing(id) {
    this.setState({
      alert: (
        <SweetAlert
          style={{ display: "block", marginTop: "-100px" }}
          warning
          showCancel
          confirmBtnText="Aceptar"
          cancelBtnText="Cancelar"
          confirmBtnBsStyle="danger"
          cancelBtnBsStyle="default"
          title="¿Estas seguro?"
          onConfirm={() => this.deleteCam(id)}
          onCancel={() => this.hideAlert()}
        >
          Esta acción no podrá ser revertida.
    </SweetAlert>
      )
    });
  }

  deleteCam(id) {
    if (id !== undefined) {
      this.setState({ blocking: !this.state.blocking });
      let fields = {
        data: { "idcampaña": id }
      };
      axios.post("https://www.ideasqueactivanclaro.com.co/admin/campaigns/delete", fields)
        .then(res => {
          var resp = res.data.response;
          this.setState({ blocking: !this.state.blocking });
          if (res.data.error === 0) {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  success
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="success"
                  confirmBtnText="Aceptar"
                >
                  {resp}
                </SweetAlert>
              )
            });
            setTimeout(this.onloadCam(), 500);
            setTimeout(this.onloadIncativeCam(), 500);
            ;
          } else {
            this.setState({
              alert: (
                <SweetAlert
                  style={{ display: "block", marginTop: "-100px" }}
                  danger
                  title=""
                  onConfirm={() => this.hideAlert()}
                  onCancel={() => this.hideAlert()}
                  confirmBtnBsStyle="danger"
                  confirmBtnText="Aceptar"

                >
                  {resp}
                </SweetAlert>
              )
            });
          }

        }).catch(error => {
          this.setState({ blocking: false });
          this.setState({
            alert: (
              <SweetAlert
                style={{ display: "block", marginTop: "-100px" }}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                confirmBtnBsStyle="danger"
                confirmBtnText="Aceptar"
              >
                No se recibió información. Inténtalo de nuevo.
              </SweetAlert>
            )
          });
        });

    } else {
      this.setState({
        alert: (
          <SweetAlert
            style={{ display: "block", marginTop: "-100px" }}
            title="Error al eliminar"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
            confirmBtnBsStyle="danger"
            confirmBtnText="Aceptar"

          />
        )
      });
    }
  }
  onloadCam() {
    axios.get("https://www.ideasqueactivanclaro.com.co/app/general/campaigns/", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const campañas = res.data.response.map((prop, key) => {
            return {
              id: prop.idcampaña,
              idCategory: prop.categoria,
              title: prop.titulo,
              textoprevio: prop.textoprevio,
              textoprincipal: prop.textoprincipal,
              img1: prop.imgprevia,
              img2: prop.imgprincipal,
              link: prop.link,
              datepublic: prop.fechaPublicacion,
              dateStart: prop.fechaInicio,
              dateEnd: prop.fechaFin,
              tipo: prop.tipo,
              posicion: prop.posicion,
              actions: (
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.campañas.find(o => o.id === prop.idcampaña);
                      this.setValues(obj);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-pencil" />
                  </Button>{" "}
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.campañas.find(o => o.id === prop.idcampaña);
                      this.onInactivateCam(obj.id, 0);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-archive" />
                  </Button>
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.campañas.find(o => o.id === prop.idcampaña);
                      this.onRemoveCampaing(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                </div>
              )
            };
          });
          this.setState({ campañas });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
            </SweetAlert>
          )
        });
      });
  }
  onloadIncativeCam() {
    axios.get("https://www.ideasqueactivanclaro.com.co/app/general/campaigns/incativeCampaigns", { headers: { "x-session": "U2FsdGVkX1/Nsa+cagdGTeOUGrUFhpmw5Tld5bg5cgTrmIMWamt6GTfB08zUPg6DQu8RztUNUU73JFZqkjwLI4PnkAffgcqFMxmDK7uMdihOH9uPNhq1fO4QGoJk4IuElpbMSKicka5CbRB0ggAUHKBw58nW1aN0GPjFElKFGp/rULwwm2WWcpQ69suBhU9kA/jV0kyphEFm5YJKYRo11tgaYuC0/i667YO/DFHR5TI=" } })
      .then(res => {
        if (res.data.response) {
          const campañasInactivas = res.data.response.map((prop, key) => {
            return {
              id: prop.idcampaña,
              idCategory: prop.categoria,
              title: prop.titulo,
              textoprevio: prop.textoprevio,
              textoprincipal: prop.textoprincipal,
              img1: prop.imgprevia,
              img2: prop.imgprincipal,
              link: prop.link,
              datepublic: prop.fechaPublicacion,
              dateStart: prop.fechaInicio,
              dateEnd: prop.fechaFin,
              tipo: prop.tipo,
              posicion: prop.posicion,
              actions: (
                <div className="actions-right">
                  <Button
                    onClick={() => {
                      let obj = this.state.campañasInactivas.find(o => o.id === prop.idcampaña);
                      this.onInactivateCam(obj.id, 1);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-check" />
                  </Button>
                  <Button
                    onClick={() => {
                      let obj = this.state.campañasInactivas.find(o => o.id === prop.idcampaña);
                      this.onRemoveCampaing(obj.id);
                    }}
                    bsStyle="danger"
                    simple
                  >
                    <i className="fa fa-trash" />
                  </Button>{" "}
                </div>
              )
            };
          });
          this.setState({ campañasInactivas });
        }
      }).catch(error => {
        this.setState({ blocking: false });
        this.setState({
          alert: (
            <SweetAlert
              style={{ display: "block", marginTop: "-100px" }}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="danger"
              confirmBtnText="Aceptar"
            >
              No se recibió información. Inténtalo de nuevo.
            </SweetAlert>
          )
        });
      });
  }
  componentDidMount() {
    this.onloadCam();
    this.onloadIncativeCam();
    this.onLoadCategories();
  }
  render() {
    const { newCampaña, preview, editCam, newCategoria, editarImgPre, editarImgPri, viewOthers, categories ,contador} = this.state;
    var today = Datetime.moment().subtract(0, 'day');
    var valid = function (current) {
      return current.isAfter(today);
    };
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              {viewOthers && (
                <Col md={12}>
                  <Card
                    content={
                      <Row>
                        <Col md={12}>
                          <h4 className="numbers text-danger media">Campañas inactivas</h4>
                          <Button className="pull-right mb-2 btn-fill" bsStyle="info" fill wd="true" onClick={this.toggleOthers}>
                            Volver
                          </Button>
                        </Col>
                        <Col md={12} xs={12}>
                          <ReactTable
                            data={this.state.campañasInactivas}
                            filterable
                            columns={[
                              {
                                Header: "Categoría",
                                accessor: "idCategory"
                              },
                              {
                                Header: "Título/nombre",
                                accessor: "title"
                              },
                              {
                                Header: "Fecha de publicación",
                                accessor: "datepublic",
                              },
                              {
                                Header: "Fecha de inicio",
                                accessor: "dateStart",
                              },
                              {
                                Header: "Fecha fin",
                                accessor: "dateEnd",
                              },
                              {
                                Header: "",
                                accessor: "actions",
                                sortable: false,
                                filterable: false
                              }
                            ]}
                            defaultPageSize={10}
                            showPaginationTop={false}
                            previousText="Anterior"
                            nextText="Siguiente"
                            loadingText='Cargando...'
                            noDataText='No hay información disponible'
                            pageText='Página'
                            ofText='de'
                            rowsText='filas'
                            defaultFilterMethod={this.filterMethod.bind(this)}
                            // Accessibility Labels
                            pageJumpText='ir a la página'
                            rowsSelectorText='filas por página'
                            showPaginationBottom
                            className="-striped -highlight"
                          />
                        </Col>
                      </Row>
                    }
                  />
                </Col>
              )}
              {preview && !viewOthers && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <BlockUi tag="div" blocking={this.state.blocking}>
                          <Row>
                            <Col md={12}>
                              <Col md={1} className="pr0 mt5" onClick={this.togglePreview}>
                                <img src={Arrow} alt="Flecha" />
                              </Col>
                              <Col md={11} className="pl0 ml0">
                                <h4 className="col-md-6 ml0 pl0 numbers text-danger media">Previsualizar campaña</h4>
                              </Col>
                            </Col>
                            <Col md={12}>
                              <h3 className="col-md-6 fwbold numbers media">{this.state.nombre}</h3>
                            </Col>
                          </Row>
                          <Row>
                            <Col md={12}>
                              <Col md={7} className="mb-2">
                                <img src={this.state.file1.base64 || this.state.img2} className="w100p" alt="242x200" />
                              </Col>
                              <Col md={7} className="mb-2">
                                <p className="category text-justify font-italic">
                                  {this.state.textoprincipal}
                                </p>
                              </Col>
                            </Col>
                            <Col md={11}>
                              <Col md={5} >
                                <Button bsStyle="default" bsSize="large" fill onClick={this.togglePreview}>
                                  Continuar edición
                                </Button>
                              </Col>
                              {!editCam && (
                                <Col md={5}>
                                  <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                    Publicar
                                </Button>
                                </Col>
                              )}

                              {editCam && (
                                <Col md={5}>
                                  <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                    Publicar
                                </Button>
                                </Col>
                              )}
                            </Col>
                          </Row>
                        </BlockUi>
                      }
                    />
                  </Col>
                </Row>
              )}
              {newCategoria && !preview && !viewOthers && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <BlockUi tag="div" blocking={this.state.blocking}>
                          <Row>
                            <Col md={12}>
                              <Col md={12} className="pl0 ml0">
                                <h4 className="col-md-6 ml0 pl0 numbers text-danger media">Crear categoría</h4>
                              </Col>
                            </Col>
                          </Row>
                          <Row className="mb-2">
                            <Col md={12}>
                              <Col md={6}>
                                <FormGroup>
                                  <ControlLabel>Nombre categoría</ControlLabel>
                                  <FormControl placeholder="Escribe un nombre" value={this.state.nombreCategoria} onChange={(e) => this.setState({ nombreCategoria: e.target.value })} type="text" />
                                </FormGroup>
                              </Col>
                              <Col md={6}>
                                <FormGroup>
                                  <ControlLabel>Icono</ControlLabel>
                                  <div className="custom_file_upload">
                                    <input type="text" disabled="true" className="file" value={this.state.iconName} placeholder="Seleccione un icono" name="file_info" />
                                    <div className="file_upload">
                                      <FileBase64
                                        multiple={false}
                                        onDone={this.getIcon.bind(this)} />
                                    </div>
                                  </div>
                                </FormGroup>
                              </Col>
                            </Col>

                          </Row>
                          <Row >
                            <Col md={12}>
                              <Col md={5} >
                                <Button bsStyle="default" bsSize="large" fill onClick={this.toggleCategory}>
                                  Cancelar
                                </Button>
                              </Col>
                              <Col md={5}>
                                <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitNewCategory} >
                                  Crear
                            </Button>
                              </Col>
                            </Col>
                          </Row>
                        </BlockUi>
                      }
                    />
                  </Col>
                </Row>
              )}
              {newCampaña && !preview && !viewOthers && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <form id="campaingsForm">
                          <BlockUi tag="div" blocking={this.state.blocking}>
                            <Row>
                              <Col md={10} className="mb-2">
                                {!editCam && (
                                  <h4 className="numbers pl15 text-danger media ml0">Crear campaña</h4>
                                )}
                                {editCam && (
                                  <h4 className="numbers pl15 text-danger media ml0">Editar campaña</h4>
                                )}
                              </Col>
                              <Col md={2} className="text-right">
                                <Button bsStyle="info" fill onClick={this.togglePreview}>
                                  Previsualizar
                          </Button>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Título de campaña</ControlLabel>
                                    <FormControl placeholder="Escribe un titulo" value={this.state.nombre} onChange={(e) => this.setState({ nombre: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <ControlLabel>Tipo de diagramación</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="singleSelect"
                                    value={this.state.singleSelect}
                                    options={this.state.tipos}
                                    onChange={(value) =>
                                      this.setState({ singleSelect: value })
                                    }
                                  />
                                </Col>

                              </Col>
                            </Row>
                            <Row>
                              <Col md={12} className="pl25">
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Categoria</ControlLabel>
                                    <Select placeholder="Seleccionar"
                                      name="sucursalSelect"
                                      value={this.state.categoriaSelect}
                                      options={this.state.categories}
                                      onChange={value =>
                                        this.setState({ categoriaSelect: value })
                                      } />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Texto previo</ControlLabel>
                                    <FormControl placeholder="Escribe un texto" value={this.state.textoprevio} onChange={(e) => this.setState({ textoprevio: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            {!editCam && (
                              <Row>
                                <Col md={12}>
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Imagen previsualización</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFile1.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Imagen principal</ControlLabel>
                                      <div className="custom_file_upload">
                                        <input type="text" disabled="true" className="file" value={this.state.imgName2} placeholder="Seleccione una imagen" name="file_info" />
                                        <div className="file_upload">
                                          <FileBase64
                                            multiple={false}
                                            onDone={this.getFile2.bind(this)} />
                                        </div>
                                      </div>
                                    </FormGroup>
                                  </Col>
                                </Col>
                              </Row>
                            )}
                            <Row>
                              {editCam && (
                                <Col md={12}>

                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Fecha inicial</ControlLabel>
                                      <Datetime
                                        locale="es"
                                        onChange={(e) => this.setState({ fechaIni: e.format("DD/MM/YYYY") })}
                                        value={this.state.fechaIni}
                                        timeFormat={false}
                                        isValidDate={valid}
                                        inputProps={{ placeholder: "Escoge una fecha", id: "fechaIni" }}
                                      />
                                    </FormGroup>
                                  </Col>
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Fecha final</ControlLabel>
                                      <Datetime
                                        locale="es"
                                        onChange={(e) => this.setState({ fechaFin: e.format("DD/MM/YYYY") })}
                                        value={this.state.fechaFin}
                                        timeFormat={false}
                                        isValidDate={valid}
                                        inputProps={{ placeholder: "Escoge una fecha", id: "fechaFin" }}
                                      />
                                    </FormGroup>
                                  </Col>
                                </Col>
                              )}

                              {!editCam && (
                                <Col md={12}>
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Fecha inicial</ControlLabel>
                                      <Datetime
                                        locale="es"
                                        timeFormat={false}
                                        isValidDate={valid}
                                        inputProps={{ placeholder: "Escoge una fecha", id: "fechaIni" }}
                                      />
                                    </FormGroup>
                                  </Col>
                                  <Col md={6}>
                                    <FormGroup>
                                      <ControlLabel>Fecha final</ControlLabel>
                                      <Datetime
                                        locale="es"
                                        timeFormat={false}
                                        isValidDate={valid}
                                        inputProps={{ placeholder: "Escoge una fecha", id: "fechaFin" }}
                                      />
                                    </FormGroup>
                                  </Col>
                                </Col>
                              )}
                            </Row>
                            <Row>
                              <Col md={12} className="pl15">
                                <Col md={12}>
                                  <FormGroup>
                                    <ControlLabel>Texto principal</ControlLabel>
                                    <FormControl
                                      rows="5"
                                      value={this.state.textoprincipal}
                                      onChange={(e) => this.setState({ textoprincipal: e.target.value })}
                                      placeholder="Escribe el texto principal de la campaña"
                                      type="text"
                                      componentClass="textarea" />
                                  </FormGroup>
                                </Col>
                              </Col>
                            </Row>
                            <Row>
                              <Col md={12}>
                                <Col md={6}>
                                  <FormGroup>
                                    <ControlLabel>Link</ControlLabel>
                                    <FormControl placeholder="Escribe un link" value={this.state.link} onChange={(e) => this.setState({ link: e.target.value })} type="text" />
                                  </FormGroup>
                                </Col>
                                <Col md={6}>
                                  <ControlLabel>Posición</ControlLabel>
                                  <Select placeholder="Seleccionar"
                                    name="posicionSelect"
                                    value={this.state.posicionSelect}
                                    options={this.state.posiciones}
                                    onChange={(value) =>
                                      this.setState({ posicionSelect: value })
                                    }
                                  />
                                </Col>
                              </Col>
                            </Row>
                            {editCam && (
                              <Row>
                                <Col md={8} className="mb-2">
                                  <Col md={6}>
                                    {!editarImgPri && (<Thumbnail src={this.state.img2} alt="242x200">
                                      <h3 className="text-center">Imagen principal</h3>
                                      <p className="text-center">
                                        <Button
                                          onClick={this.editarPri}
                                          bsStyle="primary"
                                          simple><i className="fa fa-edit"></i></Button>
                                      </p>
                                    </Thumbnail>
                                    )}
                                    {editarImgPri && (
                                      <div className="thumbnail">
                                        <h3 className="text-center">Imagen principal</h3>
                                        <div className="custom_file_upload mb-2">
                                          <input type="text" disabled="true" className="file" value={this.state.imgPriName} placeholder="Seleccione una imagen" name="file_info" />
                                          <div className="file_upload">
                                            <FileBase64
                                              multiple={false}
                                              onDone={this.getImgPri.bind(this)} />
                                          </div>

                                        </div>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPri}
                                            bsStyle="danger"
                                            simple><i className="fa fa-times fa-2x"></i></Button>
                                          <Button
                                            onClick={this.updateImgPri}
                                            bsStyle="success"
                                            simple><i className="fa fa-check fa-2x"></i></Button>
                                        </p>
                                      </div>
                                    )}
                                  </Col>
                                  <Col md={6}>
                                    {!editarImgPre && (
                                      <Thumbnail src={this.state.img1} alt="242x200">
                                        <h3 className="text-center">Imagen previsualización </h3>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPre}
                                            bsStyle="primary"
                                            simple><i className="fa fa-edit"></i></Button>
                                        </p>
                                      </Thumbnail>
                                    )}
                                    {editarImgPre && (
                                      <div className="thumbnail">
                                        <h3 className="text-center">Imagen previsualización</h3>
                                        <div className="custom_file_upload mb-2">
                                          <input type="text" disabled="true" className="file" value={this.state.imgPreName} placeholder="Seleccione una imagen" name="file_info" />
                                          <div className="file_upload">
                                            <FileBase64
                                              multiple={false}
                                              onDone={this.getImgPre.bind(this)} />
                                          </div>

                                        </div>
                                        <p className="text-center">
                                          <Button
                                            onClick={this.editarPre}
                                            bsStyle="danger"
                                            simple><i className="fa fa-times fa-2x"></i></Button>
                                          <Button
                                            onClick={this.updateImgPre}
                                            bsStyle="success"
                                            simple><i className="fa fa-check fa-2x"></i></Button>
                                        </p>
                                      </div>
                                    )}
                                  </Col>
                                </Col>
                              </Row>)}
                            <Row>
                              <Col md={12}>
                                <Col md={5} >
                                  <Button bsStyle="default" bsSize="large" fill onClick={this.toggleBox}>
                                    Cancelar
                                </Button>
                                </Col>
                                {!editCam && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitForm}>
                                      Publicar
                            </Button>
                                  </Col>
                                )}
                                {editCam && (
                                  <Col md={5}>
                                    <Button bsStyle="danger" bsSize="large" fill onClick={this.onSubmitUpdateForm}>
                                      Publicar
                                </Button>
                                  </Col>
                                )}
                              </Col>
                            </Row>
                          </BlockUi>
                        </form>
                      }
                    />
                  </Col>
                </Row>)}
              {!newCampaña && !newCategoria && !viewOthers && (
                <Row>
                  <Col md={12}>
                    <Card
                      content={
                        <Row>
                          <Col md={12}>
                            <NavDropdown
                              eventKey={1}
                              className="pull-right"
                              style={{ display: "table-cell" }}
                              title={
                                <Button className="mb-2 btn-fill" bsStyle="danger" fill wd="true" >
                                  Crear nueva
                          </Button>
                              }
                              noCaret
                              id="basic-nav-dropdown-2"
                            >
                              <MenuItem eventKey={1.1} onClick={this.toggleBox}>Campaña</MenuItem>
                              <MenuItem eventKey={1.2} onClick={this.toggleCategory}>Categoria</MenuItem>
                            </NavDropdown>
                            <Button className="pull-right mb-2 btn-fill mr20" bsStyle="warning" fill wd="true" onClick={this.toggleOthers}>
                              Ver Archivadas
                          </Button>

                          </Col>
                          <Col md={12} xs={12}>
                            <ReactTable
                              data={this.state.campañas}
                              filterable
                              columns={[
                                {
                                  Header: "Categoría",
                                  accessor: "idCategory",
                                  id: "idCategory",
                                  filterMethod: (filter, row) => {
                                    if (filter.value == "todos") {
                                      return row;
                                    }
                                    
                                    return (row[filter.id] == filter.value)?row:false;
                                    
                                  },
                                  Filter: ({ filter, onChange }) =>
                                    <select
                                      onChange={event => onChange(event.target.value)}
                                      style={{ width: "100%" }}
                                      value={filter ? filter.value : "todos"}
                                    >
                                      <option value="todos" selected>Selecciona</option>
                                      {categories.map((prop)=>{
                                        return (
                                          <option value={prop.label}>{prop.label}</option>
                                          )
                                      })}
                                      

                                    </select>
                                },
                                {
                                  Header: "Título/nombre",
                                  accessor: "title"
                                },
                                {
                                  Header: "Fecha de publicación",
                                  accessor: "datepublic",
                                },
                                {
                                  Header: "Fecha de inicio",
                                  accessor: "dateStart",
                                },
                                {
                                  Header: "Fecha fin",
                                  accessor: "dateEnd",
                                },
                                {
                                  Header: "",
                                  accessor: "actions",
                                  sortable: false,
                                  filterable: false
                                }
                              ]}
                              defaultPageSize={10}
                              showPaginationTop={false}
                              previousText="Anterior"
                              nextText="Siguiente"
                              loadingText='Cargando...'
                              noDataText='No hay información disponible'
                              pageText='Página'
                              ofText='de'
                              rowsText='filas'
                              defaultFilterMethod={this.filterMethod.bind(this)}
                              // Accessibility Labels
                              pageJumpText='ir a la página'
                              rowsSelectorText='filas por página'
                              showPaginationBottom
                              className="-striped -highlight"
                            />
                          </Col>
                        </Row>
                      }
                    />
                  </Col>
                </Row>
              )}
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ReactTables;
