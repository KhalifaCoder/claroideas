import React, { Component } from "react";

export class ProductCard extends Component {
  render() {
    return (
      <div className="col-md-12 ">
        {this.props.data.map((prop, key) => {
          return (
            <div key={key} className="col-md-4">
              <div className="pt7p">
                <div className="image">
                  {prop.image}
                  <div className="col-md-7 mb5 pl20 text-left">
                    <div className="author">
                      {prop.nombre}
                    </div>
                  </div>
                  <div className="col-md-7">
                    <div className="col-md-6">
                      <div className="col-md-12 p0 text-left">
                        <p className="text-muted mb0 fwbold fs13">Código</p>
                        <p className="description fs13">{prop.codigo}</p>
                      </div>
                      <div className="col-md-12 p0 text-left">
                        <p className="text-muted mb0 fwbold fs13">Valor c/u</p>
                        <p className="description fs13">{prop.valor}</p>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="col-md-12 p0 text-right">
                        <p className="text-muted  mb0 fwbold fs13">Cantidad</p>
                        <p className="description fwbold fs13">{prop.cantidad}</p>
                      </div>
                      <div className="col-md-12 p0 text-right">
                        <p className="text-muted mb0 fwbold fs13">Valor</p>
                        <p className="description fwbold fs13">{prop.valorTotal}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );

        })}
      </div>
    );
  }
}
export default ProductCard;
