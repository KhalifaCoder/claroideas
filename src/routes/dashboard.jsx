import Dashboard from "views/Dashboard/Dashboard";
import Notifications from "views/Notifications/Notifications";
import perfiles from "views/perfiles/perfiles";
import fabricantes from "views/fabricantes/fabricantes";
import novedades from "views/novedades/novedades";
import Campañas from "views/Campañas/Campañas";
import Noticias from "views/noticias/Noticias";
import Concursos from "views/Concursos/Concursos";
import Casas_programadoras from "views/CasasProgramadoras/CasasProgramadoras";
import Proveedores from "views/Proveedores/Proveedores";
import Herramientas from "views/Herramientas/Herramientas";
import Merchandising from "views/merchandising/merchandising";
import ReporteLogin from "views/LogsReport/LogsReport";
import UpdatedReport from "views/UpdatedReport/UpdatedReport";
import pdfCompetencia from "views/pdfCompetencia/pdfCompetencia";
import Parameters from "views/parameters/Parameters.jsx";

var dashboardRoutes=[];
var componentes={
  Dashboard,
  Notifications,
  perfiles,
  fabricantes,
  novedades,
  Campañas,
  Noticias,
  Concursos,
  Casas_programadoras,
  Proveedores,
  Herramientas,
  Merchandising,
  ReporteLogin,
  UpdatedReport,
  pdfCompetencia,
  Parameters
};
const data= sessionStorage.getItem("Cookie");
const {permisos}=JSON.parse(data);
permisos.forEach(element => {
  dashboardRoutes.push({
    path: element.ruta,
    icon: "pe-7s-angle-right",
    name: element.nombre,
    component: componentes[element.componente]
  });
});
dashboardRoutes.push({ redirect: true, path: "/", to: permisos[0].ruta, name: permisos[0].nombre });

export default dashboardRoutes;



